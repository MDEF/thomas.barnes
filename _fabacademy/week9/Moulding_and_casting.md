---
title: Moulding and Casting
period: 21-27 March 2019
date: 2018-3-21 12:00:00
published: true
---

![]({{site.baseurl}}/plantpot2.jpg)

This week was the moulding and casting week. We had the task of creating a mould from silicone rubber and then casting a part in it.

Principles of Mould Making:

- The benefits of mould making is that once you have a mould you can repeatedly produce parts, which can be far quicker and reliable than 3D printing them. You also have a far greater range of materials to cast from, from resins to food.
- We were given a choice of making a mould from wax, and CNC machining it, or 3D printing it. The CNC machine will generally give a much better accuracy and can give better quality of finish. The 3D printing method is much easier however, where you can just set up the files and leave to print. I chose for ease to 3D print my mould.
- We were actually designing initially a mould for a mould. Our ultimate aim was to cast hard parts. Therefore we needed a soft mould. It is hard to get hard part out of a rigid mould. Therefore we were making a mould firstly out of wax or PLA. Then using it create a flexible silicon mould, which is then used to cast our hard final parts. A bit confusing but looks like.

Hard Mould - Flexible Mould - Hard Casting

Things to consider:

- How is the part going to come out of the mould? If there are undercuts, then the parts are likely to get stuck in the mould and not come out.
- Include a pipes to allow the casting material in, and the air out. A riser also lets you know when the mould is full because the material comes out.
- Include registration points so the mould fits together well and you don't get misalignment and a bad split line.
- Use release agent, however if the part of rough, then it may still stick to the mould and be challenging to get out. Surface finish can make a difference.

I decided to a make a small plant pot.

![]({{site.baseurl}}/mouldcad1.png)

Part designed in fusion360

![]({{site.baseurl}}/mouldcad2.png)

![]({{site.baseurl}}/mouldcad3.png)

The mould's for printing. Working out how the positive and negative sides operate to give the hollow in the shape.

![]({{site.baseurl}}/mouldcast1.jpg)

![]({{site.baseurl}}/mouldcast2.jpg)

3D printing the moulds, they took a while so i luckily was able to do them on two different machines.

![]({{site.baseurl}}/mouldcast3.jpg)

I filled up the moulds with water to get a reasonable idea of how much silicone i would need. I still didn't have quite enough so its a good idea to add 20% extra.

![]({{site.baseurl}}/mouldcast4.jpg)

Data sheet is needs to be read , gives the ratio of silicone and hardener, and other important information.

![]({{site.baseurl}}/mouldcast11.jpg)


![]({{site.baseurl}}/mouldcast12.jpg)

Mixing the silicone, needs to be really well stirred otherwise bits of silicone might not cure properly.

![]({{site.baseurl}}/mouldcast5.jpg)

Using a vacuum to remove as much air from the silicone as possible.

![]({{site.baseurl}}/mouldcast6.jpg)

I made the mistake of not making the wall of my mould high enough above my part. This meant it was difficult to get the air out of the silicone without it overflowing. I therefore put some tape round the top to keep the silicone in the mould.

![]({{site.baseurl}}/mouldcast7.jpg)

Two parts of mould, and some small dowel pegs to register the parts

![]({{site.baseurl}}/mouldcast8.jpg)


![]({{site.baseurl}}/mouldcast9.jpg)

I used some tape to join my moulds together, however it might have been better to use clamps.

![]({{site.baseurl}}/mouldcast10.jpg)


First casting didn't work. The mould didn't fill properly. I didn't get the volume of acrylcast correct.

I didn't also make the feed hole in my mould big enough. This meant i had to use a syringe. This was time consuming and the mixture stared going hard before it had been put in the mould. The pot life is between 6-10min.

I open up the hole in the silicone with a blade so i could pour the resin in faster. 

![]({{site.baseurl}}/plantpot1.jpg)

Second time lucky!


Download:

[STL of moulds]({{site.baseurl}}/resources/planter mould 3 v10.stl)
