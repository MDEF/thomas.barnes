---
title: Electronics Production
period: 14-20 February 2019
date: 2018-2-14 12:00:00
published: true
---

This week involved making a PCB and soldering components for the FabISP. The FabISP is an in-system programmer for AVR microcontrollers, designed for production within a FabLab. It allows us to program the microcontrollers on other boards we make.

![]({{site.baseurl}}/fabisp.jpg)

![]({{site.baseurl}}/hello.ISP.44.jpg)

The layout of the PCB with the components labelled on them.

![]({{site.baseurl}}/hello.ISP.44.traces.jpg)

PNG of traces of the board

![]({{site.baseurl}}/hello.ISP.44.interior-2.jpg)

PNG of the interior of the board

I used Fab Modules, software to create the tool paths, made specially for FABlabs and the 3D milling machines they have. There are 2 Roland Monofab SRM-20s and a Roland MDX-20 here in Barcelona Fab lab. The software uses black and white PNGs images to create the tool paths. The endmills cutting away the black parts to leave the white sections.

![]({{site.baseurl}}/PCBweekSS1.jpg)

- You import the png to the FABmodule web browser.
- Select the machine (Roland MDX -20 in this case)
- Select the tool path - PCB Traces 1/64. (note endmills are in imperial, so 1/64 or 0.0156" for smaller traces which need a smaller cutter, and 1/32 or 0.0312" for the outlines)
- Check correct machine is selected.
- Speed is 4mm/s, xyz are all 0, and Zjog is clearance when moving in the air, make sure this is above 0 so the tool doesn't drag on the surface of the PCB.
- Zhome is where you want to leave the cutter after the cut.

![]({{site.baseurl}}/PCBweekSS2.jpg)

After pressing calculate, you can then see the paths that the cutter will take. More offsets means more copper will be removed, and the spaces between the traces will be bigger. Easier to solder because there's more space, but takes longer to machine.

![]({{site.baseurl}}/PCBweek1.jpg)

Roland MDX-20 machine. Also PCB in the front of the image with double sided tape on it, used to fix down the board. Make sure the bed is clean and no dust is on it, otherwise parts may come off mid-cut.

This happened and i had to cut my board a second time.

![]({{site.baseurl}}/MDX20-controlpanel.jpg)

Control panel on the computer to move the machine, set the xy zeros and load the files.

![]({{site.baseurl}}/PCBweek2.jpg)

Machine mid cut. The Roland has a crazy issue where it loses its origin between cuts. For a mill to keep it's zero between tool changes is a fundamental necessity. We had to make a hole to align the 1/32 bit when the outline was cut, which worked for this board, but wouldn't be possible if we required a higher level of accuracy.  

![]({{site.baseurl}}/PCBweek5.jpg)

All the components for the electronic lab in trays. Don't mix up the trays!

![]({{site.baseurl}}/PCBweek3.jpg)

Making a list of components. After a component is taken, tape it the page next to its code. They are tiny so be careful not to drop them.

![]({{site.baseurl}}/PCBweek4.jpg)

Board situated in a clamp to make soldering easier.

![]({{site.baseurl}}/PCBweek6.jpg)

Board half way through soldering. The most complex parts to solder were done first, so there is space and if something goes wrong you don't have to remove all the other parts. In this case it was the mini usb and the micro controller.

![]({{site.baseurl}}/PCBweek7.jpg)

Using the diagram to check where the parts go.  

![]({{site.baseurl}}/PCBweek9.jpg)

Finished board.

![]({{site.baseurl}}/homebrew.png)

I installed homebrew to make it easier to install the avr software

![]({{site.baseurl}}/AVR.png)

AVR software install using homebrew

![]({{site.baseurl}}/programmers.jpg)

The AVR ISP that i used to program my board.

![]({{site.baseurl}}/error_make_fuse.png)

I kept getting this message: make***[fuse] Error 1 coming up when i tried to program my board. I only afterwards realised i hadn't installed the avrdude software properly. I used someone else's computer and the board worked fine.

![]({{site.baseurl}}/makefuse.png)

Make hex and make fuse commands

![]({{site.baseurl}}/makeprogram.png)

Make program command. I then tested the board on my computer and it recognised it as a fab ISP. 
