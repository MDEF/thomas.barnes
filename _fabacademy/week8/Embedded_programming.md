---
title: Embedded Programming
period: 14-20 March 2019
date: 2018-3-14 12:00:00
published: true
---

This week we were looking at embedded programming.

**Programming my board**

 I used the FABisp board that i created in week 4 to program the board i made in week 6.

![]({{site.baseurl}}/echoboardprogram1.jpg)

I downloaded all the software to use with the arduino software.

![]({{site.baseurl}}/eprogramSS1.png)

I struggled to get the burn bootloader function to work, until i realised i had the wrong programmer

![]({{site.baseurl}}/eprogramSS4.png)

Using "USPtinyISP" meant i was able to burn bootloader the board.

![]({{site.baseurl}}/eprogramSS3.png)

To program the LED i had to choose the correct pin on the ATtiny44 - in this case the LED was on Pin13 so in arduino that is Pin 0

![]({{site.baseurl}}/eprogramSS2.png)

The code to check the LED works with using the blink example.
At this point the code was uploading fine, however the LED wasn't working. I realised that the LED was put on the wrong way round, i removed it and re-soldered it.

![]({{site.baseurl}}/flash_led.gif)

It works!!

![]({{site.baseurl}}/echoboardprogram3.jpg)

![]({{site.baseurl}}/embedprog2.png)

I also got the button to work. Changing the code to blink as well when the button was pressed.

Download code

[Blinkbutton code]({{site.baseurl}}/resources/button_blink_w6board.ino)

Programming in C

![]({{site.baseurl}}/binaryblinkcode.png)


![]({{site.baseurl}}/low_light_led.gif)

I tried to make the LED blink using the C code, however the LED wasn't getting the full voltage from the board. It turns on but it is very faint.

Download code

[C code]({{site.baseurl}}/resources/cscript_w6.ino)
