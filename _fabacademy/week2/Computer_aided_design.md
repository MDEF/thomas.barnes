---
title: Computer Aided Design
period: 31 January to 6 February 2019
date: 2018-1-31 12:00:00
published: true
---

**The wonderful world of CAD**

I've used various CAD programs for a long time now. I don't feel there is a need for me to explore every type of program that we could use. Instead i'm going to concentrate on the ones i want to use and get better at. As i think CAD programs become a hindrance to designing, until you are efficient and effective at using them.

**CAD programs i have experience with:**

**2D**

- **Inkscape**: I used this as software to teach people an introduction to digital design, for laser cutting in particular. A good open-source software for vector generation.

- **Vcarve**: Not strictly speaking a design package. I used it with my CNC machine to generate tool paths. However it does have vector drawing tools inbuilt, so you can design in it if you want, although only good for basic tasks.

- **Corel Draw**: This software we used as a print driver for our laser cutters. I rarely if ever designed in it, but i know how to operate the interface and modify vectors in it.


- **Illustrator**: This software i am the most familiar with. I used it regularly for design work, and for creating laser cut designs i find it good. It is creatively a great piece of software. Where you can generate designs very easily. It does fall down a bit with a lack of parametric ability. Making adjustments to designs a bit challenging.

**3D**

- **Pro-engineer**: This is the software i learnt CAD on back at university. It was made by Autodesk, a parametric modeller great for product design and engineering. Fusion360 grew out of this software.

- **Sketchup**: This software is easy to use, and good for quickly showing concepts that are made from flat shapes. Good for simple furniture design, layouts and buildings. Struggles with more organic shapes and patterns.

- **Rhino**: I have only used rhino a small amount, however i am familiar with its operation. I can create basic things. It's good for ideas, and fast to model. You can also make things at all scales easily.

- **Fusion360**: This is the software i will focus on in fabacademy. As mentioned above i used to be very familiar with Pro-Engineer. I understand the principles of how to model in it, even if i don't know all the functions really well yet. My aim is to get faster at using it and more familiar with the more complex aspects of the software.

CAD Modelling this week:

I created a design for a circular material library, i did it first in Illustrator then in Fusion 360.

**Illustrator Sketch**

![]({{site.baseurl}}/CAD week1.png)

I first created a circle

![]({{site.baseurl}}/CAD week2.png)

I created a rectangle for the slots

![]({{site.baseurl}}/CAD week3.png)

Then I used the rotate tool to pattern the rectangle

![]({{site.baseurl}}/CAD week4.png)

Pathfinder tool and divide to separate the shapes

![]({{site.baseurl}}/CAD week5.png)

I removed all the divided vectors not required to get the circle with slots

![]({{site.baseurl}}/CAD week6.png)

I then added another rectangle to show the sample boards, and patterned again using the rotate tool

![]({{site.baseurl}}/CAD week7.png)

Colour and text added to finish the 2D sketch

![]({{site.baseurl}}/CAD week8.png)

**3D model**

![]({{site.baseurl}}/CAD week9.png)

I started a sketch a created a circle, i then offset another one inside

![]({{site.baseurl}}/CAD week10.png)

I then created a rectangle for the slots

![]({{site.baseurl}}/CAD week11.png)

A radial pattern created the slots around the shape, trimming the parts not needed.

![]({{site.baseurl}}/CAD week12.png)

Extrude to thickness of 18mm

![]({{site.baseurl}}/CAD week13.png)

Part copied and moved the right distance apart.

![]({{site.baseurl}}/CAD week14.png)

The sample board created in another sketch, with slots included

![]({{site.baseurl}}/CAD week15.png)

Part extruded and moved up to the top slot

![]({{site.baseurl}}/CAD week16.png)

Part in place

![]({{site.baseurl}}/CAD week17.png)

I added fillets to the corners and used the radial pattern again to copy the boards

![]({{site.baseurl}}/CAD week18.png)

Patterned boards with front on frame

![]({{site.baseurl}}/CAD week19.png)

Board removed to show how it would slot in to the frame

![]({{site.baseurl}}/CAD week20.png)

I then created a shaft coming out the back of the central plate. Then a block was made that would hold the shaft.

![]({{site.baseurl}}/CAD week21.png)

I created two profiles that would be used to create a loft for the legs.

![]({{site.baseurl}}/CAD week22.png)

The rail for the loft caused me a couple of issues, making sure it was placed centrally in the profiles

![]({{site.baseurl}}/CAD week23.png)

The loft means i can have different shaped profiles that blend into each other, and a changing cross section in the leg.

![]({{site.baseurl}}/CAD week25.png)

I then mirrored the leg to get the other half.

![]({{site.baseurl}}/library_front_render.png)

Rendering of the front, using fusion 360 in built renderer

![]({{site.baseurl}}/library_back_render.png)

Rear view

Download Files

[Fusion360 file]({{site.baseurl}}/resources/Materials_wheel_v9.f3d)

[Illustrator file]({{site.baseurl}}/resources/MaterialLibrary.ai)
