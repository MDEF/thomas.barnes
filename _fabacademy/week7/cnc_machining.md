---
title: CNC Machining
period: 7-13 March 2019
date: 2018-3-7 12:00:00
published: true
---

![]({{site.baseurl}}/scooterhero.jpg)


This week we were CNC machining. Making something big! We did the task in pairs, and me and Nico decided to make a scooter. Nico's documentation of the process can be found [here](https://mdef.gitlab.io/nicolas.viollier/fa/w8/)

![]({{site.baseurl}}/scootss1.jpg)

![]({{site.baseurl}}/scootss3.jpg)

![]({{site.baseurl}}/scootss2.png)

I designed the scooter in fusion360. The benefit of this software is that we could add joints with constraints and test whether parts would turn or not.

![]({{site.baseurl}}/scootss4.jpg)

![]({{site.baseurl}}/scootss5.jpeg)

We laser cut a scale model of the scooter. The material for CNC machine is 15mm plywood, so therefore we scaled down the model by factor of 5 and used 3mm plywood.

![]({{site.baseurl}}/scootss6.jpeg)

The joints all had fillets added, as the CNC router bits can't machine sharp internal corners. The bit we used was 6mm, so the fillets had to have at least a 3mm radii

![]({{site.baseurl}}/scootss7.jpeg)

Parts nested in Rhino before creating tool paths.

![]({{site.baseurl}}/scootss8.jpeg)

Checking the simulation that the tool paths are correct. Using Rhino Cam

![]({{site.baseurl}}/scootss9.jpeg)

Gcode for the screws toolpath. This tool path is make sure that the screws that fix the board down don't get in the way of the cutter.

![]({{site.baseurl}}/CNCmachine.jpg)

The CNC machine we used is a RaptorX - SL. It's a 3 axis machine with a cutting area of 3020mm (x axis) by 2010mm (y axis). 

![]({{site.baseurl}}/scootss10.jpg)

Parts cut on the CNC machine

![]({{site.baseurl}}/scootss11.jpg)

We made a mistake with the slots on the handle, when we made a change. As a result the handle was the wrong way round.

![]({{site.baseurl}}/scootss12.jpeg)

The front part was too wide for the wheels. so we had to add some blocks that supported the axle and prevent it bending.

![]({{site.baseurl}}/scootss13.jpeg)

The front part was too wide for the wheels. so we had to add some blocks that supported the axle and prevent it bending.

![]({{site.baseurl}}/scootss14.jpg)

Download:

[Rhino file of parts]({{site.baseurl}}/resources/Scooter CNC Rhino Cam_correctecd.3dm)

[CAM file for screws]({{site.baseurl}}/resources/1-screws-6o.nc)

[CAM file for parts profiles]({{site.baseurl}}/resources/2-holes-profiling-6o.nc)
