---
title: Electronics Design
period: 28 February to 6 March 2019
date: 2018-3-6 12:00:00
published: true
---
This week we had a chance to design our own circuit boards.

We used the echo hello world board design.

![]({{site.baseurl}}/heroboard.jpg)

I used Eagle to create the schematic for my board. It first involves importing the correct library of parts. Which are the ones available in the fablab electronics lab. We then select the parts, and connect them together, labelling them and giving the correct values.

Parts:

- **6-pin programming header**
- **Microcontroller: Attiny44A**
- **FTDI header**
- **20MHz crystal**
- **Resistors** - 0 ohm resistor the jumper. 499 ohm resistor for the LED, 10K ohm resistor for the button
- **Capacitors** 2 x 22 pf capacitors
- **Button**
- **LED**
- **Ground**
- **VCC**


![]({{site.baseurl}}/eagleSS_echoboard1.png)

Using add part function - getting the parts from the fab library of components

![]({{site.baseurl}}/eagleSS_echoboard3.png)

Laying out the schematic and starting to attach nets

![]({{site.baseurl}}/eagleSS_echoboard4.png)

![]({{site.baseurl}}/eagleSS_echoboard5.png)

Changing the name of the nets to connect components together

![]({{site.baseurl}}/eagleSS_echoboard7.png)

Importing the parts into the board layout to begin arranging them.

![]({{site.baseurl}}/eagleSS_echoboard9.png)

![]({{site.baseurl}}/eagleSS_echoboard10.png)


 Once all the parts are connected together comes the tricky part. Getting everything laid out neatly and in the best arrangement. It's a good idea to arrange the parts in the least tangled way before beginning to route the traces in place. Rotating them and changing their position. Ideally there should no lines that cross each other. However if there is no way to avoid it, then 0ohm resistors can be used as jumpers, to bridge the gap over a trace. However if the layout causes too many to be put in, then start again and try and get better routes.

 ![]({{site.baseurl}}/eagleSS_echoboard6.png)

 I had to add more parts as i realised i'd left some out.

![]({{site.baseurl}}/boardlayout.png)

I managed to get away with one jumper, and probably could have got a neater layout as well but I kept it as it was.

![]({{site.baseurl}}/Week6 Traces.png)

Image of the exported layout of the traces. I had to check the size of the image, because there is a bug in the Mac version of Eagle i have, and it exported initially too large. I used to photoshop to resize it.

![]({{site.baseurl}}/electronicdesign2.png)

Using Fabmodules to create the tool paths for the traces and outline.

![]({{site.baseurl}}/electronicdesign1.jpg)

Parts being machined on the Roland MDX-20

![]({{site.baseurl}}/electronicdesign3.jpg)

Finished cut!

![]({{site.baseurl}}/electronicdesign4.jpg)

Again like doing the previous board I stuck all the components to a page before starting soldering

![]({{site.baseurl}}/electronicdesign5.jpg)

Board in the clamp before components

![]({{site.baseurl}}/electronicdesign6.jpg)


![]({{site.baseurl}}/electronicdesign7.jpg)

Board finished, just needs programming now.

It was at this stage i realised i had put a wrong component on my board, a capacitor instead of a 0 ohm resistor jumper, not sure how i did that. However, i took it off and replaced it with the correct component.

Download:

[Traces PNG]({{site.baseurl}}/resources/Week6_board.png)

[Outline PNG]({{site.baseurl}}/resources/Week6_outline-01.png)
