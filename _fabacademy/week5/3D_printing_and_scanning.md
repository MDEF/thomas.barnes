---
title: 3D Printing and Scanning
period: 21-27 February 2019
date: 2018-2-27 12:00:00
published: true
---

![]({{site.baseurl}}/cup.JPG)


This week we were 3D printing and scanning items. I've done both before, but it was still good to use the equipment in the lab here in Barcelona


![]({{site.baseurl}}/scan2.JPG)

We set up an Kinnect for Xbox 360, and tried scanning some small items.

![]({{site.baseurl}}/scan3.JPG)

The Kinnect was put on a base up high, and items on a rotating platform to try and get even a scan as possible.

![]({{site.baseurl}}/scanprint4.png)

However this didn't really work as the Kinnect isn't really designed to scan small objects. This was supposed to be a wooden lion, but it doesn't have a head, and the mesh is really messy.

![]({{site.baseurl}}/tommodel3.png)


I scanned myself instead! Wearing a very cool hat and headphones. I sat on a swivel chair and this helped get a better mesh.

![]({{site.baseurl}}/scanprint1.png)

**3D Printing and Principles**

- Design model in 3D CAD software - i myself used Fusion360
- Make sure the model is water tight! No open surfaces, or the model won't print properly.
- There are different types of software that will slice up models before 3D printing them, i used Cura, which is the software made my Ultimaker. Although you can use other custom machines with it.
- Time is an important factor in printing, high resolution models will take significantly longer than lower ones. Resolution is dependant on nozzle size and layer height. Layer height will normally be between 0.15mm and 0.3mm. The initial layers tend to be thicker to give the models a good base.
- Choose a material depending on the application. PLA is popular because it is easy to print with mostly made from renewable sources. ABS works at higher temperatures and is stronger. Nylon is good for applications needing low friction. Temperatures for printing will provided my the filament manufacturer.
- If there are over hangs - where angles of parts are between 45 degrees and 90 degrees then the model will need support structure. This is sometimes frustrating because it can spoil surface finish when it is broken off.
- Shell is the thickness of the wall, and can be changed depending on the design.
- Infill is the amount of material inside the model, generally between 20-90% and it affects the strength of the model, and the time taken for it to print. There are different layouts the infill can take depending on geometry.
- The first few layers are the most important to check, to see if the model is printing properly. If the bed is the right height from the extruder, and that the part doesn't lift off the bed.

For the 3D printed assignment i decided to make a small cup.

![]({{site.baseurl}}/scanprint5.png)

I downloaded a Voroni sketch generator plugin for Fusion360 to help create the pattern i put on the cup.

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/scanprint6.png"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/scanprint7.png"/>
  </div>
</div>


![]({{site.baseurl}}/veroni_cup.jpg)

![]({{site.baseurl}}/ultimaker2.jpg)




I decided to use the Ultimaker 2 for my printing.

![]({{site.baseurl}}/cura_ss.jpg)

I used glow in the dark PLA for the filament, and i had to put in support, as the pattern was creating some overhangs.

![]({{site.baseurl}}/printingcup.jpg)


![]({{site.baseurl}}/scanprint10.jpg)

Printer mid-print

![]({{site.baseurl}}/scanprint11.jpg)

The print had support that had to be removed.

![]({{site.baseurl}}/scanprint8.jpg)

There were still some parts that need cleaning up, as the strands drooped and make the model untidy.

![]({{site.baseurl}}/scanprint9.jpg)

Download:

[Fusion 360 cup file]({{site.baseurl}}/resources/veroni cup.f3d)

[STL cup file]({{site.baseurl}}/resources/veroni cup.stl)
