---
title: Project Management
period: 23-30 January 2019
date: 2018-1-23 12:00:00
published: true
---

**Website Time**

This week we created Fabacademy sections to our websites. We had already set up sites for our MDEF documentation, however creating a new section site wasn't completely straightforward.

**Gitlab and Jekyll**

I use atom as my text editor, and hyper as my terminal. Atom is very easy to use and has the staging, commit, push and pull functions all built it.

![]({{site.baseurl}}/atom1.jpg)

![]({{site.baseurl}}/git-process.jpg)

The steps to push files to the archive are as follows:

- Create a profile in gitlab
- Start a project which will act as the repository.
- Choose a location on your computer and clone the repository.
- To add files / save changes to the repository - stage all using atom.

![]({{site.baseurl}}/pushprocess.png)

- Files are added to the staging area.
- A commit message is then added to show what the changes being made are.
- Commit to master then adds them to a queue, waiting to be pushed.
- Pushing sends them to the Gitlab server.
- As long as the changes are passed the website should then show the changes made.

The websites I used at the beginning of MDEF was based on an MKDoc template. It was good for documenting, but not very good for doing any personalisation. For this reason I changed to using Jekyll. A static site generator based on Ruby Gems.

I was given another template to use by [Ilja Panić](https://mdef.gitlab.io/ilja.panic/). It has all the capability to adapt and change my website to suit my needs.

![]({{site.baseurl}}/jekyllstarter.jpg)

A feature i find really useful is the *bundle exec jekyll serve*. It creates the site and makes it available on a local server. This means i can test my website without having to push anything up to the main Gitlab server, which makes iterations much faster.

![]({{site.baseurl}}/hyper_jekyll_serve.jpg)

**Adding the Fabacademy section**

This required adding the Fabacademy home page, an html documentation, with the code for all future posts to be listed.

![]({{site.baseurl}}/fabacademyhome.jpg)

I then created Fabacademy folder, where weeks are included and a project management markdown file created. The title indicates which page is listed, and the period gives the date and orders the post list.

![]({{site.baseurl}}/pmheader.jpg)

Links were put in both the header and the second term sections on the home page of the website.

![]({{site.baseurl}}/headerlinks.jpg)

![]({{site.baseurl}}/linkicons.jpg)

![]({{site.baseurl}}/linkiconbutton.jpg)


Lastly the config file was changed. There were a few attempt to get this right. Adding another collection wrongly meant that the reflections from the first term couldn't be found, then i lost the sass layouts for the old posts as well. Eventually i found the right way to get both paths to work correctly.

![]({{site.baseurl}}/config.jpg)


I will add a new section each week, as i do each week's documentation.

**My Fabacademy Project**

My project aim's to look at sustainability in Fablabs themselves. How to increase material literacy, apply more circular design approaches, reduce waste and environmental impact.

My aim is provide a tool kit of practical approaches for Fab Lab users.

More details can be found [here](https://mdef.gitlab.io/thomas.barnes/reflections/design-dialogues/)
