---
layout: page
title: About
permalink: /about/
---


![]({{site.baseurl}}/images/3DprintedTcopy.png)


Hi I am Tom and i'm a Designer and Digital Fabricator from Bristol, England.

I trained as an Industrial Designer, and have worked for the last 9 years in Prototyping and Digital Fabrication. I am interested in how digital fabrication and other emerging technologies can be used to promote social and environmental change. I helped set up a maker space in Bristol called KWMC:The Factory working within local communities providing a making service. I also ran workshops introducing people to the technologies we had.

I want to explore how digital fabrication technologies fit within larger industrial processes. Particularly within the area of sustainable materials. For example how can materials be reused more effectively and circular systems adopted. I also want to look into how new approaches to design, materials and waste are being fed into other areas of society.


When not working I love being outdoors, cycling, food and music.
