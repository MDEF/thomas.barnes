---
title: 12. Design Dialogues
period: 17-23 December 2018
date: 2018-12-17 12:00:00
icon: 'images/Design-Dialogues-13.svg'
term: 1
published: true
---

This week was the culmination of a terms work. We had to present our areas of interest and potential ideas for intervention. Guests came to discuss the work we were presenting, give their input for concepts we could explore in the next two terms.

**Are Fablabs environmentally sustainable? How can we tell?**

![]({{site.baseurl}}/display1.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg2.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg3.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg4.jpg)

<br>

{% figure caption: "*Material flow animation*" %}
![]({{site.baseurl}}/video-2.gif)
{% endfigure %}


![]({{site.baseurl}}/Presentation-pg6.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg7.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg8.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg9.jpg)

<br>

![]({{site.baseurl}}/Presentation-pg10.jpg)


Notes from the guests that I spoke to during the presentation.

**Guillem and Mariana**

- Fab lab need to envision life cycles of products - what do people do with the products once they have been made
- Interaction with raw materials
- New design methodology for Fab lab users - design a tool kit
- Have a test case and measure it
- Don’t be too abstract - make sure you have a concrete thing to test
- Challenge relationship designers have with their products
- Could you ask people to design a fablab, but it needs to have a material zone.

**Ingi**

- This is a really difficult project, he’s been trying to improve recycling for years.
- It is a learning environment where people will always test their projects on cheap materials, and prototyping is iterative.
- Standard prototyping material - which can be reused. This method is a bit communist and you won’t have one do all material.
- Social impact of project - does is offset the environmental impact
- VR learning? An option for prototypes or models that will use a lot of materials and have a very short shelf life.
- In the end just reduce materials used.

**Elisabeth and Lucas**

- Try out more futures techniques - like maps and scenario building.
- I will need to be more provocative and challenging to challenge the status quo.
- There are two types of change - doing behaviour (on a personal level) and instilling behaviour (in others). Create stories and try and install behaviour change in others.
- Narrow down.

**Jonathan and James**

- Labelling system on materials to give people more information on the materials that they are using
- Material driven design and short circuit information flows
- Does digital fabrication limit material choice - yes often it does, as you need materials that will fit with the processes
- Examine these three aspects behaviour, culture, expectation: Little challenging in norms
- E.g. take a new material like ‘chips board’ rather than chip board, depends on the quality of the products. However can there be a culture shift to introduce new options and people realise they should use these new products.
- You can change people, machines and materials. How can we affect these transition points?
- Scaled step change analysis
- Play with framing the question - E.g. How can tell if fab labs are sustainable? Just walk into one and have a look at it! Are there better questions?
- What can be designed to create more awareness, what can be designed to transform the library of materials??
- Frame works and transitions
- Changes in policy. Does there need to be a custodian of a material library.
- Don’t design a system, design strategies to affect it.
- Try looking at what could be done in different time frames - day / week / month. Make it scaleable.
- What resources would be needed?
- Find a point to make a change
- System capture.
- Connect globally, use the network. Manageable and buy in required?
- Labelling materials which when used had a colour added so you could see the cycles and as a consequence the degradation.

**General points**

- How can others experience your future, not just one question.
- Not just one question but ask lots
- Frame design problem and don’t obsess over solutions
- When struggling to decide think about what do you want to learn?
- Self editing - generate as many ideas as possible

**Reflections**

I have spent the whole term having a fairly clear idea of the area i want to work in, and how i want to do it in Fab labs. I will have to actually work out exactly what i am going to do next now. Dive deeply into this area and try out different things. I will need to challenge behaviours and not just end up making a better system for the bins. I'm starting to think that the first stage I identified as an option might not as much of a focus as the material usage. However this is something for future Tom to explore over the next few weeks. I also have a great opportunity to examine how material use and sustainability topics are addressed in Shenzen maker spaces and design studios.    
