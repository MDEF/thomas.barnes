---
title: 03. Design for the Real Digital World
period: 15-21 October 2018
date: 2018-10-21 12:00:00
term: 1
icon: 'images/dfdw-03.svg'
published: true
---
**15 - 19 October 2018**

This week we were tasked with redeveloping our studio space. We had to make the space more fit for purpose, and use reclaimed materials we found on the street. There also had to be elements of digital fabrication in all our designs. Whether this be CNC routing, laser cutting or 3D printing.  

<span style="color:teal">**Group Ideas**

We split into groups of 6 or 7. Making sure there were people with various skills in the group. Including people who could 3D CAD model, document, present, physical making and manual labour.

We firstly assessed the space, and came up with ideas for what improvements could be made in it. Ideas were clustered into some main areas

Maker Place - Mini FabLab area for making items in the studio. This would need various hand tools, a 3D printer and some electronics equipment e.g. soldering irons.

Ideas space - Area to put up ideas and information about work. E.g White boards, pin boards, hangers and shelves .

Modular space - A mixed use space area where people can talk, have project meetings, relax and maybe even store items. It wants to be modular and flexible and so can be arranged different ways. Depending on how the space is being used at the time.

Light and air - Improvements to the light and air in the space. This could be achieved using different lighting and including plants to oxygenate the space. e.g Ideas for a vertical garden on the pillars was discussed.


<span style="color:teal">**CAD Models + Sketches**

We decided to focus on two areas. The Maker Place and Modular Space. We split up at this point to different tasks. I modelled the space and we thought about how the layout of furniture could go. We then started working on ideas for both modular seating and the mini fab space. I came up with ideas for corner seating, benches made from doors and standing height desks. We also had ideas for flexible wall storage, triangular modular seating and other furniture.

![]({{site.baseurl}}/Ideas_image_montage.png)

Once these ideas had all been presented we took votes on what each group was going to make. There were some great ideas, all using the reclaimed items in really inventive ways. The groups were tasked with making a coffee / mushroom station, a movable room divider, shelving, modular work stations and relaxing area on the balcony. Our group needed to make a mobile Fablab and fixed station for the 3D printer.

**Mobile FabLab**: Our main item was a moveable workstation. We decided to put in on castors with good brakes. It needed to be large enough for two people to work on. At standing height. Have storage for tools and project work. The top needed to be sturdy and easy to work on. We found a very solid table top, that was already laminated, to use as a work surface. The base was from MDF, the legs from pine and the shelves from chipboard.

Some of us worked on the CAD models, while some others worked on the layout of what needed to go on or in the workstation, including accessories like a lamp, boxes, clips and peg boards.

The top, shelves, legs and base were all made using the CNC router. However there was a bit of confusion on which CAD versions were being used for the design. Some parts didn’t fit properly unfortunately, and so had to be modified, and so it didn’t go to together in time for the Friday’s deadline.

![]({{site.baseurl}}/mini_fab_lab.png)

**3D printer table**: We decided this table should be static, as 3D printers need to have a very stable platform to print properly. I took my idea of making a tool bench out of doors, and moved it to this table. Using one door for the top, and another for the legs. Then using some reclaimed door frames as bracing.

The door for the table top we cut using the circular saw. The door for the legs was put on the CNC machine, to cut out the profiles. This proved tricky however as the door was full on metal pins which actually broke an old CNC cutter. We decided therefore to just engrave a shallow groove into the surface, and then use it as a guide to cut out the leg shape using a jigsaw.

![]({{site.baseurl}}/door_table.png)
*Door cut into legs for the 3d printer table*

Once the legs were made however there was concern that they just weren’t strong enough, particularly where we were missing the strength from the door frame. This required a change of direction and we decided to use the plastic legs from an old outdoor table. We could fix the legs directly into the table top because the door was made from MDF skinned over a cardboard inner. We therefore created blocks with pockets that the legs could slot into. I did this by taking pictures of the legs, tracing the profile then checking the main measurements. We created blocks for both the top and bottom of the legs, to stop the legs spreading. They were also braced at the bottom.

![]({{site.baseurl}}/3DP_Leg_photos.png)


A laser cut cap was created as well to cover the exposed edges of the door. As the table is used we can check its stability, and add more bracing if necessary. We can also 3D print items that are needed for the machine.

![]({{site.baseurl}}/DFDW_photomontage1.png)

<span style="color:teal">**Learning processes**

**Creativity**: This project required a lot of creative thinking, not only to think of ideas, and solutions for the space, but how to use materials that were not always in a state which made them easy to use. Then when things go wrong, creativity is needed to find a solution.

**Discover**: I’ve spent a lot of time around digital fabrication equipment now. I’ve learnt though that all machine’s have there own special way of working. It takes a bit of time to get used to each one. It was good to spend a bit of time in the space this week, getting to know how everything in the FabLab operates. It is very different from my old workshop.

**Explore**: I was able to explore areas of Barcelona, looking for items to use in the studio. Different areas have different items, which in turn reflect the neighbourhoods themselves. The work that happens there and the people who live there. Then we tried to use these items and connect them to our space.

**Collaborate**: We had people from very different backgrounds in our teams. This meant that we had to work together to try and get all the tasks done. I’m used to designing in a more solo way, so I learnt a lot from having to take other people’s ideas and points of view.

**Transform**: The idea was to take discarded items and transform a space. This can be seen in a space which feels so different from the one we had to start with. We also have a much stronger connection to it. We will continue this process and we ourselves ‘transform’ and change through out the year.

**Experimentation**: This was also an opportunity to experiment with things we hadn’t tried before. I wasn’t sure if the using the door would work, and it didn’t, but I still got the opportunity to try. I could do things differently in the future to use them more successfully.

<span style="color:teal">**Reflections**

**Team work**: This week the project was very similar to work I’ve done over the last few years. I understand all of the processes, machines and materials. This should have been an easy week for me, however it wasn’t. I know this was partly because of how the team worked. I was reluctant to do too much, wanting to give other people an opportunity to practice skills like designing, using the CNC machine and fabricating. I thought I could guide and support more. There ended up being a lot of issues however, resulting in work that was replicated, confused and over complicated.

I’ve learnt a lot about how I will behave if the situation is repeated. I think the roles should have been better defined at the start, playing more to people’s strengths. Also dividing up the work better, so people weren’t repeating what others had done. Also I could maybe have been more assertive at times when I knew from experience that a direction we were going in wasn’t the right one. We were maybe over ambitious, and didn’t really consider how long it sometimes takes to make things. Particularly in a new space, getting used to other people, and sharing resources. It was a shame not to get everything finished with everyone else. However we will complete the two main items.

**Using reclaimed items**: I’ve used reclaimed items in the past. I feel like the week has confirmed issues I’ve had in the past. What is different is now I have a chance to maybe think of ways round some of them. The main issue I found getting materials was how much work has to go into making them usable at times. Removing metal, glue, finishes. Then there is access to items. You could come up with a great design for a material or item, then loose access to it. Another issue can be the resulting aesthetic of a product. A lot of the products in the studio look like they have been cobbled together from street trash, which in the time frame we had is not a surprise. I actually liked the look of the products, however a lot of people would want products that a more refined, finished and that belong together.

**The Digital Fabrication Process**: One thing found working in a digital fabrication space is that people can get a bit fixated by it. It is of course massively useful, and creates amazing items that would be impossible my hand. I find sometimes though it is used at the detriment of other processes. I feel like there should be equal value given to non-digital tools. Whenever you make something you always want to use the best tool for the job. However we sometimes used a CNC router to cut square objects. A saw would’ve been much better. Again I also realise the aim of this week was to get used to using the digital fabrication equipment. I’ve noticed that people who can make things by hand often have a better understanding of how to make things, they then can use the machine’s to advance their work. This is a personal reflection I’ve just come across before and would be interested in exploring a bit more. Particularly when introducing new people to these environments.

**Connection to Future Work**: One of my main interests in coming to do the course here was to look at how to use materials differently. I’ve worked with a lot of new materials, and I’ve also created a lot of waste when making furniture, signage and exhibitions. I want to look at how we can reduce material wastage in general. However I’m focussing at the moment on digital fabrication in particular because it’s an area I know about.

In terms of using reclaimed materials and the issues of supply, one person in my group had a great solution. Ola created a parametric model of the mini fab lab. When using it you would be able to put in the size and thickness of material you had. The model would scale accordingly, and parts generated to fit what you have. There are other designs and scripts that worked similarly, however a lot require particular materials e.g. plywood which is stronger than for example chipboard.

I also touched on access reused materials over the two posts, and how to find them. I need to look at other people e.g. Transfolab doing similar work. What networks there are, where to find materials and when. We just hunted in the street but could it be improved??

Areas to look into:

- Material supply - location and access
- Material processes - In fabrication
- Material usage - In its life as a product
- Offcuts and waste products - What can be done with whats left over e.g. sawdust
How the processes all connect.

Useful links
- [Curro Claret](https://www.curroclaret.com) - multipurpose brackets for joining reclaimed materials
- [Ikea Hackers](https://www.ikeahackers.net) - Upcycling and hacking ideas for ikea products
- [Thomas Dambo](http://thomasdambo.com) - Giant sculptures made from reclaimed wood
- [Yanko Design](http://www.yankodesign.com/2018/06/22/stunning-wood-but-not-rigid-seating/) - Seating system made from old bike inner tubes and plywood offcuts
- [Newspaper wood](https://www.yatzer.com/new-material-NewspaperWood-Mieke-Meijer-Vij5) - Wood like material made from old newspapers
- [Inhabit](https://inhabitat.com/clever-recycled-furniture-made-from-undesirable-materials/) - Furniture made from undesirable materials
