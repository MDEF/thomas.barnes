---
title: 04. Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
icon: 'images/profile-04.svg'
term: 1
published: true
---
**22 - 26 October 2018**

This week we were looking at ourselves as designers, and where we want to get to in our Masters. We started by visiting various designers in Barcelona, hearing about what they do, and how they got there. We could then reflect on ourselves in comparison to these varied profiles.

October 22nd 2018

Ariel Guersenzvaig

**Designer / Teacher / Manager / Researcher**

Ariel currently works Elisava, Barcelona School of Design and Engineering running Master’s programmes in Design and Web Direction. He is a researcher in design methodology and design decision making. He’s also an interaction designer and has worked in this field since 1996.

Ariel described his career which moved definitively from one role (Designer) to many roles (Designer, Teacher, Manager and Researcher). He shows a profile which has had to adapt to changes technology and society, but also his own interests as well. Changes in technology have been huge in the last 20 years or so. When he started in web design the internet was only in it’s infancy. This boom times of this industry meant he was in a good position until the .com bubble burst in early 2000s. He was a partner in Claro Studios, working on user experience design for some big clients. After his company was bought, he described a difficult time when he had to make some choices about his future.

This combination of external and internal pressures affect us all and what we decide to do. Ariel actually started doing a PHD in decision making, in the area of design methodology. He challenges the purely rational approach to design, saying there is a place for more intuitive / naturalistic outlook. This however does need to be backed up by knowledge and experience. Designers don’t always know why something does or doesn’t work, however if they can visualise or prototype it, then they have a medium to express their ideas. Designers actually need to create designs, because as they analyse their work they understand the problem more. This approach resonates strongly for me, as someone who needs something in front of me. To have a basis of an idea and then start working out how to find a solution.

Ariel continued to examine decision making by researching ethics, and discussed with us artificial intelligence and it’s role in society.  He prompted an interesting discussion about killer robots, and the implications of autonomous weapons systems making their own decisions on who to target. AI cannot yet replicate human recognition, as there is no robot currently who can successfully tell if a pizza is vegetarian. However the issues of responsibility makes this a much deeper issue than just technical. Ariel showed a real passion for not just designing but going into detail on what makes a designer. He’s interested in the philosophy behind it as a profession. Although he does admit he feels rusty when designing current projects. It gives me some comfort that even someone with his vast skills and experience can also suffer from ‘imposter syndrome’ on occasions.


October 23rd 2018

Sara De Ubieta

**Architect / Craftswoman / Researcher / Teacher**

Sara is a trained Architect, who is also a hugely skilled and experience shoe designer. She started working in architecture in but suffered from jobs being lost in the 2007 / 08 recession. However she had a passion for shoes and was taught the trade by a craftsman shoe maker. She reflected on how she so appreciated being able to design and maker her projects. The physicality of it, and being able to handle the materials, she relished the connection.

In traditional shoe making the primary material is leather. From various animals. Most traditional shoe makers won’t even consider other materials. Leather is a very appropriate material for shoes, due to its physical properties. However Sara in particular has an issue with it being a vegan. It also raises questions about how we use materials, that although can be very suitable to a task, do have other problems e.g. sustainable or ethical considerations. Sara became interested in how using leather could be improved. There are lots of offcuts produced in the shoe making process. She hated to see the waste so designed shoes that were made from the offcuts.

She started to work for an agency and produced shoes with small factories. Small factories are producers of shoes, not research facilities. Only large brands readily do that. As a result she struggled to get some of her creations made, if they differed from techniques the producers knew and understood. She confirmed the need for knowledge and experience when trying to push boundaries, as it can take a lot of persuasion to make people behave in new ways. She worked in fashion for 3 years but wasn’t entirely happy, she was working on her own with not that much time for research.

She began doing lots of research into materials in 2016. How they are used and combine. She draws on her background as an architect by using materials like wooden beams and insulation material. When a designer what you have done before will always influence what you are currently doing. She did comment on her lack of chemistry knowledge, which she misses when working with new materials. When asked about doing this kind of work, and making a living from it she confirms it can be a challenge. She gets funding from universities and the art world. If you want to be a  producer of shoes like this, and sell your work. It is really tough.

Workshop: Making shoes from insulation material. Using unusual materials to create designs.

![]({{site.baseurl}}/Gabor_makingshoe.jpg)

![]({{site.baseurl}}/Shoes.jpg)


October 24th 2018

Domestic Data Streamers - Pol and Marta

**Designers / Artists / Engineers / Researchers**

Domestic data streamers (DDS) is a “strategic communication consultancy firm with a mission to tackle the challenges society faces with storytelling through data”. It aims to create “meaningful connections between data and people”. In a society that is overwhelmed by data and information, how we interpret and use the data we have is getting more and more important. DDS does this by mixing lots of different disciplines, but that have a core design approach running through them all. We met both Pol and Marta. Designers who have taken different directions since training, but still have that core ‘design’ background. Marta studied design, and worked with Pol on some of their installation projects. She then went onto study engineering because she wanted to know how things worked. It was this journey that prompted her interest in doing research, which she currently does at DDS. The mix of social scientists, anthropologists, creative technologists and other backgrounds provides a basis to tackle the complex reality we are currently living in.  

I’m really interested in the storytelling approach that DDS takes. They described a project they did with Unicef called the Time Machine. This project aimed to highlight the lack of data there is on many children round the world, and how this is problematic as learning the what children’s lives are like now will be a great help in predicting what their live will be like when they are adults. DDs created a box which isolated users from the outside world. It then provided questions about the users childhood, and created a song from their answers. It then cut the music and went silent. This science aimed to represent the lack of voice many children have in the world. This exhibit was installed in a UN convention, and had a lot of coverage. I think it sounded like a powerful statement piece, and it caused many people to consider it’s message. Pol wasn’t sure of the impact at the UN convention itself however. This does highlight the challenges of getting messages and stories converted into actionable change, which surely was the ultimate aim.

Workshop: Making the invisible visible

This was a fun exercise in story telling. We all looked at what was in our pockets, and had to create a visualisation about something we found. I found the cap that goes on the headset of my bike, which I took off when trying to fix it. I visualised the decision making process I have to make when thinking how much work I should do on my bike. We had had lots of different stories coming out of the group, from the life of a lighter at a festival, to what pencils would be thrown from someone’s pencil case. The exercise showed us how there is a lot of information about ourselves on us all the time, and everything has a story behind it.

![]({{site.baseurl}}/Workshop_DDS.jpg)

![]({{site.baseurl}}/Cycle_visual.jpg)


**Areas of convergence in the profiles**

- **Ariel, Sara, DDS**: Adaptable approach, multi-disciplinary backgrounds and influences.
- **Ariel, Sara, DDS**: They are all researchers.
- **Ariel, Sara, DDS**: Questioning. Not just the world and whats in it, but design itself. The processes of  design.
- **Ariel, Sara, DDS**: Impacts of design on society - e.g. Design responsibility, AI, sustainable materials, and up-cycling.
- **Ariel, Sara**: Had to start teaching to supplement income.
- **Sara, DDS**: Involved in ‘art’ world as well as design.

**Areas of divergence**

- **DDS**: Don’t seem to be working at a university lecturing.  
- **Ariel**: Strongest academic / theoretical approach
- **Sara**: Very hands on, creating designs with her hands - physical connections.

How it links to me as a designer

What I like in the profiles I saw

- The connection to made objects. I love creating physical objects and I find it far easier to identify problems and solutions if I have something in front of me, also you get to touch and feel the materials, and get a reaction from it.
- Multidisciplinary approaches to the work we saw. People often trained in one things and then moved to something else, however they retained their previous influences. I think this gives a much richer base to work from.
- Pulling intangible ideas into the physical world. Enabling people to see ideas, interact with difficult concepts, and emotionally react to them. E.g. The time machine from DDS.
- I like the approach which not only questions the design themselves you are working on, but the process itself. Examining the methodology of designing and decision making itself.
- I like the need to self develop, to be more skilled and better informed. This means that they have are in a better position to challenge and push boundaries, because they are doing so from a position of knowledge.
- That they considered the impact of design, how designers have a responsibility to be aware of the effect of their work.
- I liked the different ways of communicating with people. Engaging with people in fun and interesting ways is important. Particularly if the topic itself has the potential to be pretty dry and boring.
- I liked the approaches that used lots of different tools to communicate messages. Whether it be visual, audio, interactive or live acting. Having physical objects, websites, songs, apps and people. Not just limiting to one medium.


**Exercise**

What I have and what I’d like to have.

<br>


| What I have                                                                                                      | What I want to to have                                                                                                                      | How to get there (Term 1 of MDEF)                                                  |
|------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------|
| Visualisation skills - sketching and CAD skills.                                                                 | Better at researching - Be better informed in theory, particularly in the area of material waste, circular economy and fabrication.         | Navigating uncertainty.                                                            |
| Making skills - Prototyping skills and Digital Fabrication                                                       | Design ethics - impact of design and its role in the world. Particularly in sustainability when it comes to material waste and up cycling   | Hybrid profiles in design. Navigating uncertainty. Introduction to futures.        |
| Projects based very much in the real / physical world. Material knowledge and techniques to produce them.        | Have more of a design methodology. Why I make design decisions.                                                                             | Hybrid profiles in design. Designing with Extended Intelligence. Living with Ideas |
| Communication. I can communicate ideas in a written, visual and physical ways.                                   | How to take big issues and scale them down to manageable chunks. Test them in a small way, then help them grow.                             | Living with Ideas. The way things work. From bits to atoms.                        |
| Teaching people. I taught people design and digital fabrication skills.                                          | Engage with people in different ways. Improve communication and interaction design. Particularly in graphics, photography and video making. | Living with Ideas. Design Dialogues.                                               |
| People skills - Working with people from different backgrounds, also ones not necessarily from the design world. | Designing systems and platforms, not just stuff in the physical realm.                                                                      | Hybrid profiles in design. Living with ideas.                                      |
| Social and community engagement. Getting projects into communities that they act in.                             | Be better at presenting ideas, more confident in public speaking and arguing my position.                                                   | Engaging narratives. Design Dialogues.                                             |
|                                                                                                                  | I want to be more of an activist. Promote actionable change in the area of material waste and upcycling.                                    | Hybrid profiles in design. Engaging narratives.                                    |
|                                                                                                                  | Getting a project funded / financially viable.                                                                                              | Engaging narratives                                                                |
|                                                                                                                  | More materials knowledge. Their background and uses.                                                                                        | Biology Zero. Design for the real digital world. From bits to atoms.               |
|                                                                                                                  | Programming knowledge. To be able to design interactive installations to help convey ideas and messages.                                    | Hybrid profiles in design. The ways things work.                                   |

**Creating a direction:**

Having a direction is important when projects are complex and constantly changing. You need to remain constant whilst everything else is moving. We used three articles to show how this can be done by as a designer having a vision, reflections, and experiences.

Articles

Vision - [Designing for the Unknown](https://www.researchgate.net/publication/254941682_Designing_for_the_unknown_a_design_process_for_the_future_generation_of_highly_interactive_systems_and_products)

![]({{site.baseurl}}/reflective_design_process.png)

Reflection - [Through Annotated Portfolios](http://interactions.acm.org/archive/view/january-february-2013/annotated-portfolios-and-other-forms-of-intermediate-level-knowledge)

Experience - [Designing for, with or within: 1st, 2nd 3rd person points of view on designing for systems](https://dl.acm.org/citation.cfm?id=2399045)

<br>


| Vision               | Reflect                | Experience               |
|----------------------|------------------------|--------------------------|
| Learn from mistakes  | Annotations            | Breaking barriers        |
| Gathering data       | Examples               | Design as a stakeholder  |
| Build a vision       | Guidelines             | Personal motivtion       |
| Integrate approaches | Critiques              | Other peoples motivation |
| Open design          | From tacit to explicit | Real need                |
| Flexibility          | Tools and methods      | Not imposing             |
|                      |                        | Openess and flexibility  |
|                      |                        | Being the real context   |
|                      |                        | Making transformations   |
|                      |                        | Capabilities.            |


**What is a vision?**

![]({{site.baseurl}}/vision.jpg)

**My vision for my work**

Despite training in Industrial Design I’ve seen myself as more of a maker than designer over the last 10 years. Working in prototyping and digital fabrication. One thing that I’ve always struggled with is the waste we produced while making things. I love the process of creating a physical object, realising your ideas, being able to touch them and interact with them. The linear approach to creating products that still exists at the moment however is one that I strongly dislike. I want to investigate work people are currently doing to tackle this issue and find opportunities to support them.

**Draft vision**

“To promote a circular approach and increase access to sustainable materials in designing for digital fabrication processes. To use digital fabrication as a model for how other systems can achieve zero waste and promote the message of sustainable design to wider audiences.”

I currently don’t know how exactly I’m going to do this. I just feel there is a lot of focus on the technical capabilities of digital fabrication processes. However the waste is a big weakness in the process for me. Projects like precious plastics are good examples of existing work being done in this areas. I want to see how these projects could integrate and connect together.

There are three possible avenues I could look at currently.

- Materials
- Processes
- Systems

**Reflections**

Key themes:

- *Specificity*: A vision needs to be clear in it's aims. Vagueness makes it easier to achieve something, but without focus it will be harder to actually follow a path or direction.
- *Flexibility*: When designing something complex the approach requires flexibility. The ability to change direction easily, and move around problems quickly. Being too rigid will constrain and limit results.
- *Iteration*: A vision will be a constantly evolving idea, something that is worked on as you progress, know more, and see things differently. This is not to say it should be limited if something goes wrong, but built on as experiences are learnt from.
- *Responsibility*: A vision has to be grounded in reality and experience. This doesn't mean you have to know if something is possible, but is has to be based in a known reality.
- *Sharing*: Once you have a vision then it is important to share it, because then you can get other points of view, this can be supportive or challenging. However you can use others experience and knowledge to help achieve your goals.


An interesting video on the importance of visioning.
[Dr. Dana Meadows: Envisioning A Sustainable World](https://vimeo.com/13213667)
