---
title: 01. MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
icon: images/boot-01.svg
published: true
---

**1 - 5 October 2018**

This first week of Mdef Bootcamp felt like a great introduction to the varied and interdisciplinary nature of the course.

We began on Tuesday by introducing ourselves and talking in more details about who we are, why we are here and what we would like to get out of the course. I had met most of the people from the course already, but I still found it amazing to hear the mix of backgrounds, experience and skill sets. On the first day I realised this combination would be integral to the success of all our times here. We have communication designers, industrial designs, architects, fabricators, researchers, a web developer, an economist, an engineer and a doctor (phd). Everyone has skills, interests and experience that can be brought into both group and individual projects.

There were some key themes that were identified that people were interested in working on. There were more but they included:

Community activism -
Community engagement -
Participatory design -
Democratising Technology -
Urban Farming -
Education -
Circular processes in design and digital fabrication -
Sustainability and issues of consumption -
Repurposing city and urban models.

We split ourselves into working groups, that all shared a rough common interest. My group all have an interest in ‘changing process’, with a strong theme of sustainability running through it. We want to look into how Digital Fabrication can be made more circular and how it can connect better with larger industry? We also want to look into materials that can support a less linear approach and how we can make it easier for people to access these technologies.

**Where we see ourselves in 10 years?**

We were asked where we see ourselves in 10 years, which was an interesting exercise, mainly because none of us were particularly sure. I was surprised at how many people were doing the same thing as me and using this course to focus their direction. I have spent many years in prototyping and digital fabrication. I love making things, but I also feel like I need to move beyond just being ‘on the tools’. I want to look bigger, see how I can engage in larger issues in society, using the skills and experience I have. I see this year as a way of exploring different options, discovering new methods of working and how things could work. I have worked as a designer on the technical side of projects, and also on their social and community aspects. It sometimes can be difficult to bridge the two, unless you are from quite a interdisciplinary background. I hope I can take on that role more. I would be interested in setting up my own project, and being able to work and run it how I want. However I would want to do it with some like minded people. I don’t think it is easy to work in isolation, great work often comes with combined creativity and support.

**Gitlab**

On Wednesday we had a session in using Gitlab to create our websites. This was a bit of challenge as I have not done any work creating websites or used Git. However we were persuaded that it would be very useful later on, particularly in doing group work. Git’s version control, and ability to have multiple workers making changes simultaneously is very powerful. Having a good conflict management process will definitely be helpful. With support I managed set up my website, and change some aspects. However it will be a steep learning curve using html and markdown. I can change text and add images, next step is to change layouts to something I prefer more. I will be adding to the website every week, so it’ll be good to see it develops over the year. Mariana gave us information on using Git, but also on using information in general. One key point which stuck was making sure our sources are solid, and we know where the information is coming from. Is is biased? Is it true? Who paid to get the information?

She also provided some useful websites to support how we use data:

- [Carto](https://carto.com)  - Location data service
- [Canva](https://www.canva.com/)  - Graphic design website
- [Infogram](https://infogram.com/)  - Inforgraphic creation
- [Tableau](https://www.tableau.com/)  - Interactive data visualisation
- [Thenounproject](https://thenounproject.com/)  - Icons and symbols for everything

**Tomas Diez**

We had presentation from Tomas, our course director on Thursday afternoon. I’ve known of Tomas since 2015 when I saw him talk in the London FabLab. I really enjoyed hearing about all the work he was involved with and its kind of crazy that now I’m doing a master’s he’s running! It was great to hear more of his history and the origins of how his work began. The fact that cities can’t support themselves is a massive issue. The aim for Barcelona to be at least 50% self sufficient by 2050 is one that I would like to take back home with me, see if cities in the UK could follow.

**Exploring Poblenou**

On Thursday afternoon we went out into Poblenou itself, on a ‘City Safari’. We walked around seeing businesses and projects that might connect with our own work. It is a very creative neighbourhood. With a lot of ‘hidden’ activity, on the surface Poblenou seems quiet, at least compared to other areas of the city. However there is a lot going on here.
Here are some of the places we saw / heard about

Pujades 97

Social firm collecting e-waste. They use learning software and image recognition to form an inventory of all the parts they collect. This means people can find specific parts for their equipment. It also trains local unemployed people.

Leka

Open source restaurant. This is a project that Tomas worked on with the owners. Everything from the furniture to the food is open source here. You can download it from their website and make it yourself. A successful business reflecting the open community.

{% figure caption: "*LEKA Sign outside restaurant*" %}
![]({{site.baseurl}}/LEKA.jpg)
{% endfigure %}


Indissoluble

Architecture studio, however it really does a lot more varied work making exhibitions, installations and events. They have all the design and manufacture on site. Including CNC routing, Laser cutting, 3D printing and wood and metal fabrication.
They have an open doors night on 23rd November

{% figure caption: "*Giant brain installation, using coloured LEDs to display brain activity*" %}
![]({{site.baseurl}}/Brain.jpg)
{% endfigure %}

Transfolab

A design, innovation and experimentation space dedicated to making things out of trash! A membership funded maker space. They have a wood, metal and ceramics workshop.

{% figure caption: "*Transfolab sign outside their workshop*" %}
![]({{site.baseurl}}/tranfosign.jpg)
{% endfigure %}

{% figure caption: "*Group visit inside the workshop*" %}
![]({{site.baseurl}}/transfo_inside.jpg)
{% endfigure %}

**SuperBlocks**

The super block plan aim’s to reduce cars in blocks in city. Keeping fast roads on the outside of the blocks, and having the centres for pedestrians and cyclists. This is to reduce noise and pollution, make it safer for children. It also reclaims the areas from cars and making it easier to have events, playgrounds and green spaces. There has been a lot of opposition from local people, particularly businesses who think that without easy access by car their trade will suffer.

{% figure caption: "*Tomas explaining about SuperBlocks*" %}
![]({{site.baseurl}}/Tomas_superblock.jpg)
{% endfigure %}

{% figure caption: "*Seat car dealership with posters opposing the plan*" %}
![]({{site.baseurl}}/Seat_no.jpg)
{% endfigure %}

**Biciclot**

A social firm which focuses on bikes to support local communities. They sell remade bikes, they train people in bike maintenance so they can keep their bikes in good condition. There is a mobile bike workshop they can use to reach communities. They also hold events and support educational projects.

{% figure caption: "*Our presentation about biciclot*" %}
![]({{site.baseurl}}/biciclot_talk.jpg)
{% endfigure %}

{% figure caption: "*The workshop space where they were fixing bikes while we were there*" %}
![]({{site.baseurl}}/Biciclot_workshop.jpg)
{% endfigure %}

**Waste Collection**

After visiting the different projects and businesses. We went hunting! In our groups we went out and sought materials that could be used to transform out studio space. In Barcelona there are evenings in each area where you can put out rubbish or unwanted items for people to collect. You can find all sorts of things, from pallets to chairs, packaging and fridges. There are even people who make a living scavenging the more valuable items, and moving them around in shopping trolleys. Our interest was more in materials that could be used within a digital fabrication space. Our group found lots of dibond, offcuts of valchromat, a plywood pallet, and a timber frame.

**Oscar Tomico**

On Friday afternoon out other course director Oscar gave us a presentation on some of his work. He has done a lot of work on wearable technology. Combing soft materials like textiles with hard materials like electronics raise some interesting challenges. We need to be open to problems and frustrations, that can lead to new insights. We often design for things we know and understand, but then constrain our work. It is also very important to live your designs. While they are just ideas or drawings, how can we know how they make someone feel? It is great to use our bodies as a tool, to get experience, feedback, context and know where to move next. Words can also be a challenging medium in which to express ourselves. However materials can often say so much more.

**Area Mapping**

Mariana printed out a large map of Poblenou and she started to stick on the places we’d been on our tour and other areas of interest for us. This will a really useful tool because it grounds what we are doing in reality. We can’t just create ideas in the classroom. We need to take them out into the real world. Using the area around us is a good place to start. We can work out how the different places connect, how links can be formed, and projects supported. Using the area and the city as a lab will give us so much more insight that just theoretical ideas. We will continue to add to the map as we discover more places and make connections between them.

**Group discussions**

I think that some of the most interesting ideas on this course will come out of group discussions. Everyone comes with different points of view and so we will all be challenged by each other at different times. We did an interesting exercise in reflecting on what our assumptions / expectations had been before this week were, what the reality was like and how to move forward in the future. There were lots of discussions about IAAC, Poblenou and our working interests.

**Peer to peer lessons / support**

Everyone is very keen to support and help each other, which has been great. We all have different skills and can learn of each other. For example Ilja is a web developer and gave us an additional lesson in Git, which was massively helpful to try and get our heads around the basics. There is a WhatsApp and Facebook group where people are also sharing links, ideas and interesting things to see and visit. In terms of additional learning I think this adds so much to the value of the course

**Learning process**

This week i can break down the learning I've done into these areas

- **Absorb** - I took on all sorts of new information, from learning Gitlab to where Bike projects happen. Processing and making sense of it all.
- **Question** - Why did i work in certain ways before? How can i look at things differently? Why do neighbourhoods feel like they do? How could we improve existing systems? Who is already doing similar work?
- **Suggest** - Ideas are starting to form on the direction of the projects. Using systems that already exist to support exploration. Adding to networks and connecting local systems.
- **Share** - This website is the first step in sharing ideas, however there is also sharing of knowledge with other course mates and our groups will benefit from shared experience.
- **Reflect** - There always needs to be time to think back on how we did things. What worked and didn't. One key thing i've learnt from this week is to document as i go along, and not leave it all to one day. Stay organised and improve next time around.  

**Reflection**

I think the messages of context in how we work is very important. We learnt so much more from going out into the neighbourhood of Poblenou, rather than just hearing about in a presentation. For example seeing how people reacted when we were looking for discarded materials. Some people were supportive. However some people particularly older generations didn’t understand, thinking we didn’t understand how the rubbish collections worked.

However going out into the neighbourhoods makes me realise how little I know about this area. I’m not from this country, city or neighbourhood which I will be working in. This means that I don’t understand a lot of the context behind how people behave here. This might be important in the future. I will therefore have to learn more about its history. However there is a limit to how much I can learn in a matter of months, plus I will always be an outsider. This makes it more important and potentially valuable to embed interventions within existing infrastructures. Find people who are already doing similar activities and connect with them.

I found the work at biciclot fascinating, as I love bikes and also have worked within similar bike based projects back at home. I wonder if we could combine bike projects with some of the material systems we will be looking into, although there are limits to what can be transferred by bike.

There is already an informal but loosely structured system for repurposing unwanted items in the city. This also supports a hidden economy for people who collect some of the items. I’m sure there are already systems in place for people to obtain waste items, and somewhere like Transfolab is a good place to start looking into networks. Identity other projects doing similar things, find out weaknesses in systems but also opportunities. Find out what business to business opportunities there might be as well. I found in my own work it was often easier/quicker/cheaper to use new materials rather used ones. How could this be challenged with current processes available to us is something I’m really interested in. There are lots more places and projects in the area and in the city that I don’t know about (well most of them currently). As our project develops I will need to spend time researching what other places could be useful.

As a neighbourhood Poblenou doesn’t advertise its contents very loudly. However there are open days which would good to visit and connect. There is a big one on November 23rd where places will open their doors, and show what they do and provide beer. It’ll be an opportunity to network.
