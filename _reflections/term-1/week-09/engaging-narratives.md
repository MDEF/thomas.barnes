---
title: 09. Engaging Narratives
period: 26 November - 2 December 2018
date: 2018-12-02 12:00:00
term: 1
icon: 'images/engage_nar-10.svg'
published: True
---

This week we engaged with our own stories, and the journey we are taking on this course. How we describe, promote, and generally communicate our work has a massive impact. A great story has the ability to affect the direction of a project. We are good at recognising a well told story, to craft one ourselves is much greater challenge.

**The Power of Stories**

Telling stories is something fundamentally human. They touch on almost every aspect of our lives. We tell stories everyday, teach, learn, inspire and influence others with them. They are a medium of communication as old as any we have. They are a leveller and something that we can use to find commonality when there are many differences. Our brains interpret stories in a much more holistic sensory way than just absorbing raw data or facts. This means if you want someone to retain information, telling them a story. The more convincing the story, with appropriate details of context, emotion and action, the more likely someone will be to remember it. Better yet people might change their actions because of a well told story. They can promote actionable change on a personal level, and as the stories spread this can be expand to much wider audiences.

The power of stories has been known for a long time, and they can be used in many ways. They can be used to influence and control behaviour in positive but also negative ways. They have been themes in dystopian novels like “Fahrenheit 451” where books are banned, and are burned by ‘fireman’. Similarly in George Orwals “1984” history is literally rewritten by those in power, to the point where no can be sure of the actual past because no accurate record remains. We can also see affects of this today. Whether it be stories targeting people using social media, to the repetition of certain themes in main stream media.

In Chimamanda Ngozi Adichie's TED Talk [“The danger of the single story”](https://www.ted.com/talks/chimamanda_adichie_the_danger_of_a_single_story?language=en#t-1110124) she discusses the dangers of when stories are one sided. They are incomplete, and this can lead to stereotypes. The problem with stereotypes isn’t that they are always wrong, its just that they are so reductive. A person, group or race can be limited to a couple of characteristics which in no way does them justice. It can lead to assumptions and actions that may have major consequences.

<br>

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/lang/en/chimamanda_adichie_the_danger_of_a_single_story" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

**Say it with Confidence**

A story needs to engage and resonate with people. In this case it needs to be told with a confidence that makes people believe it, or in it. We looked at various forms of self promotion this week. A key issue for me has often been fully trusting the validity of my ideas. However if i don't have faith in a story i'm telling, how will anyone else? Particularly if i'm trying to influence a habit or promote change.

**Please be Insightful**

Sometimes the stories we tell are filtered. This problem happens a lot on social media, where the filters can literally be added. We see only a very edited version of someone’s world. There is a tendency for self promotion, and despite the ability to connect us, ends up with people feeling more divided. Those that have, those have not, those that can, those that can’t. We had a workshop Bjarke Calvin who promotes another form of social media, which he describes as ‘Insight media’. In this app people tell stories, and from the stories they also pass on their insights. What they learnt, and what what the subject means to them. This has the ability for knowledge to be passed on, insights shared and built on.

Example: The Time I Made Bin Smoothies

![]({{site.baseurl}}/duckling-bin-smoothie.png)

This story is about when i helped out with a project all FoodCycle, which aims to tackle food waste. The story is as follows:

- A group of us used our bikes to collect food from shops that was being thrown away but was safe to eat.
- Four of us collected over 120kg of food in one morning that was destined for the bin.
- We took it all to a local community centre where we used it to create stews and smoothies. The food was very fresh so had to be used that day. However it made amazingly tasty food that otherwise would've have been wasted.

My insights here came from the fact i hadn't fully understood that sometimes the food that we rescued could be better than the food we normally buy. Only because of timing. We normally would've eaten the food or maybe thrown it away before it got to the sweet spot where it was actually perfect to eat. We are so disconnected from our food that unless there is a label we often don't know when it is just right to consume it. People always loved hearing about this project because it tackled so many themes including food waste, food poverty, social isolation and green transport.

There is potential for this to lead to some interesting content. People trying things, learning from them, triumphs and failures. I will be interested to see where the app goes, because there could be a tendency for people to share content that is not really very profound. People might try and misuse the platform, and share misleading, abusive or incorrect information. However that is the case in any technology that it can be used in ways that they’re not intended. It would require a community of users who care enough to self manage, and an ability to filter out the content that was deemed inappropriate. Stories have impact and I think that platforms e.g. Facebook do have an obligation to be aware of the content they share on their sites. Hopefully Duckling will be an alternative with a better ethos than some of it’s predecessors.

**My journey**

As part of the week’s work we were asked to write a story that described our MDEF journey, and at the very least described our area of interest. I decided to describe the motivation I had for my particular project, and made up a fictional solution of a recipe book of methodologies to support sustainability in Fablabs. I added a product after our Kickstarter lectures with Heather Corcoran. Despite not really thinking it was going to be a direction I want to take, I thought it would be easier to test out creating a story with a tangible solution.

<br>

<iframe width="800" height="480" src="https://www.youtube.com/embed/fwvYhCG34J4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Reflections:

- I’m not an animator and haven’t used after effects that much, so in the circumstances I’m pleased with result, and that I gave it a go.
- The animation is a bit clumsy, in its execution and content. It’s more a description of my process than a story, with a quite a bit of text. In an ideal world it would have a much more playful feel, and more of a narrative described in the animation itself.
- I hope the rather catholic feel of my video doesn’t offend anyone. I just thought it would be an amusing image, and it showed me trying to shift my direction of work in response to increasing environmental issues, and a personal response to them.
- As I mentioned earlier I don’t currently feel a recipe book on its own will change much, so isn’t going to be a direction I will choose.

My story currently lacks a focus, i still have lots of directions that i could follow, and the task for the immediate future is to take all the interests i have and distill them down into a direction that will achieve the aims i have been looking at for the past term.
