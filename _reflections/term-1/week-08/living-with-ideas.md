---
title: 08. Living with Ideas  
period: 19-25 November 2018
date: 2018-11-19 12:00:00
term: 1
icon: 'images/living_with_ideas-08.svg'
published: true
---
**19 - 23 November 2018**

This week’s aim was to make aspects of our projects ‘real’. We needed to quickly live with our work and gain insights from our experiences. We had presentations and workshops run by Angella Mackey and David McCallum. They introduced methodologies which we can use in our projects, by mainly using ourselves as test subjects. This was an opportunity to go and do something related to our projects. It’s very easy to keep ideas in your head or just on paper, however moving them into the real world is where real insights come. You also encounter issues that force you to engage with the aspects you may or may not have predicted.

**Workshops:**

Speculation and Counterfactual objects:

The first exercise involved using speculative design to create an object. This object is modified to reflect the reality you are speculating about. This then gives you the ability to interact with it and gain some insight about the reality you are trying to envisage.

We were all told to bring in objects, and lay them out on a table. We all chose one and then started to think up ‘what if’ scenarios that could relate to the object. I chose a hairbrush, and I thought about what this object would be like within the scenario: ‘What if we all lived in space’. I thought of various things that it could relate to, like washing your hair, holding items in zero gravity, but in the end I came up with it as a health monitor, with the bristles as sensors that read your health as you moved it over your skin. I attached three LEDs to the head of the brush, and tabled a cell battery to the back to light one of the LEDs and simulate it working. The indicators were red for ill, yellow for ok, and green for healthy.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/spec_objects_all.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/Brush_with_leds.JPG"/>
  </div>
</div>

We had to then live with this object, and one of the immediate issues I had, was that I’m not currently in space, so it’s very hard to simulate that context. I had to subtly change the use of the brush, so it became a health monitor to check whether you were healthy enough to go into space, but used while still on earth. I then tried it out with other people in the room. Testing out their ‘health’.

{% figure caption: "*Emily checking her health*" %}
<div class ='center-image'>
   <img src="{{site.baseurl}}/emily_brush.jpg"/>
 </div>
{% endfigure %}


Reflections:

Technical points on the design
- It was great having a device that’s totally non-invasive, just wave it over your body and it tells you if something is wrong.
- I didn’t have a very good feedback mechanism, the lights told me something was wrong but not what the problem was.

The experience:
- It lets you react to something, and know very quickly if a design works in a context using yourself as feedback.
- You can refine aspects of it.
- You can share the idea with other people.
- To trust our ideas.
- It is a way of simulating a technology that doesn’t currently exist.

Group work:

I really enjoyed the task of thinking what if? The task of imagining different scenarios, sometimes good, sometimes bad. Forcing us to consider what life would be like if a key element was different in some way. We also came up with some ideas in groups, as some of have shared interests and common themes. Some of the scenarios we came up with:

<p align="center">
What if we were responsible for all out own waste?<br>
What if we had to provide materials to before we bought anything?<br>
What if we didn’t own anything?<br>
What if we had to grow our own clothes?<br>
What if we can only make with items nearby - km zero?<br>
What if all products have to be made from one material?<br>
What if we only have bacteria as tools?<br>
What if you can only make stuff out of waste?<br>
What if the most abundant resources were the most expensive?<br>
</p>

Wearing Digital Fabric

This technique involved taking an idea and using and auto-ethnographic approach to reflect on an experience and gain some insight from it. In the case of this workshop we used a green screen app to wear ‘Digital Fabric’. This allowed us to simulate wearing photos or videos.

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/Tom_stones.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/Tom_vending.JPG"/>
  </div>
</div>

![]({{site.baseurl}}/ollie_emily_tom.JPG)


Reflections:

This is another way of simulating an experience that cannot doesn’t technically exist. A way of testing out theories and influencing assumptions about future behaviours. This particular investigation was very fun, however we didn’t really pass through the ‘gimick’ stage. It was particularly fun to try and turn ourselves invisible. This in part was due to the limited time of the exercise. However it does highlight possible behaviours, and unforeseen consequences of some technologies.


Materialising an Idea

For this exercise we were given an attribute about being human and we had to make a ‘magic machine’ which reflects that meaning, and some how achieve it. My fist word was

‘Saving - the need to accumulate stuff’.

I reacted a bit to this statement. Maybe in part due to my own experiences of having too much stuff. I designed ‘The possession thief’. A windmill of containers that selects the things you have that you actually ‘need’. It keep them and gets rid of the things you don’t. A reaction in my part I suppose to the over materialistic nature of our society at the moment. There is no need for everyone to own the same items, all the time. Especially when a lot of them are infrequently used e.g. DIY Tools. The rise in [Thing Libraries](https://www.libraryofthings.co.uk) echoes this concern.

![]({{site.baseurl}}/Me_and_thief.JPG)

We repeated the same exercise with another word. This time we tried to reflect it to our projects or areas of interest. My second word was

Tranquility - The need to feel secure

I decided to follow the issues around sustainability, and how the future is so uncertain due to our lack of ability to live in a sustainable way. This uncertainty reduces our feelings of security and in turn inhibits our ability to feel calm. My magic magic machine this time round is a ‘calm compass’. It is on the one hand a stress toy, to be squeezed when feeling anxious. It also has a compass which points you in the right direction. The magic here is that it may not be the direction you want to go in, however it will lead to a destination that will provide security, calm and eventually with that tranquility.

![]({{site.baseurl}}/calm_compass.JPG)

<br>

<div class ='center-image'>
   <img src="{{site.baseurl}}/me_and_compass.JPG"/>
 </div>
 
<br>

Reflections

The ability to give material form to abstract ideas is a powerful one. This brings up the concept of labels again, and the importance of defining things carefully. In the both exercises I tried to assume an issue that I felt needed addressing. However these are definitely my take on these definitions. Someone else may have a different point of view. Having a physical object again brings life to the ideas and encourages discourse.

**My project:**

Practical experiment:

Making a sawdust cake

Steps:

I found a recipe online - Materiom.<br>
I bought the ingredients I could find.<br>
I collected sawdust from the Fablab vacuum cleaner.<br>
I went home and cooked the material in my kitchen.<br>
I brought it back to IAAC and left it to dry.<br>

Reflecting on the experience:

- I rushed the process, and didn’t have time to get all the ingredients to the exact specification. Next time I could plan better.
- My housemates were confused initially at why I was making a sawdust mixture in the kitchen.
- While cooking it. I found it exciting, actually creating something. Similar feeling to making anything for the first time. I’ve wondered about doing it for a while, and never did, so actually doing it felt like a minor achievement.
- I was also full of doubts whether it would work, and whether it would be a waste of time. I wondered if I’d just have a blob of sawdust.
- The recipe is actually quite forgiving, and i’ve got a block of slightly flexible sawdust. It has the consistency of a cake, and is not what I’d called structurally strong.
- It is still drying, so it will be interesting to see how it behaves when it is fully cured, the recipe gave no indication of how long it might take.
- I always expected a substance more similar to chip board, however it is much softer and more rubbery in texture. Which makes sense because of the agar.
- I found it interesting I keep bringing it back to food, looks like a flap jack, and joking about it.
- My least favourite activity in the workshop was emptying the saw dust from the extraction, its a messy and unpleasant job requiring a mask. How would people feel if they had to use it as an ingredient regularly??


Opportunities:

I need to consider what these materials could be used for in a new context, it might not work just trying to make them back into what they were for (like with plastic).
Can the sawdust material be used for other applications in the lab?? E.g moulded into packaging for projects…

Issues:

Far more sawdust is produced on a daily basis than would be easy to turn into a new material. It is also a fairly risky material to store long term, as it is flammable and in some cases explosive. This would need a fast processing time, which would be quite time consuming.

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/cooking_sawdust.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/cooking_sawdust2.jpg"/>
  </div>
</div>
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/cooking_sawdust3.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/Sawdust_balcony.JPG"/>
  </div>
</div>

![]({{site.baseurl}}/Sawdust_cake_cut.JPG)

**Reflections**

We spend a lot of time thinking about ideas, discussing ideas, writing down ideas. However I get the aim of this week when it comes to dragging thoughts out into the real world and seeing what happens when you wave them around a bit. It was quite a challenging process at times to distill all the different aspects of our projects into something we could live with. As a maker i am used to creating physical objects, however everything needs to be defined before you start, or it unravels pretty quickly. In this case though you are using the act of making or doing something to assist the process, so you don’t have a lot elements defined at all.

I feel like I’ve framed my issues quite well, however i’m still unsure about the direction of using sawdust to make materials. I think it’s a useful exercise to see what is possible, and I’ll run with it a bit longer to see where it goes. I think though that there will need to be more attention given to the beginning of processes when it comes to waste. Sawdust cakes might deal with the problem but they do little to challenge the causes. A focus on the materials being chosen in the first place could have more impact than just cleaning up after the ones being currently used.

Sawdustcake update:

I checked the sawdust cake on 30th November, 1 week after i made it. I had been storing it in my locker. It had gone very mouldy. This is an interesting development and i will need to look into the cause of this. It obviously is an issue if a new material grows mould.
- However it could be interesting to see if it could grow other organisms?
- Could it be used as a filler for another substance?
- Could it be a processed or eaten by mycelium?

{% figure caption: "*Not such a tasty cake*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/Mould_cake1.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/Mould_cake2.jpg"/>
  </div>
</div>
{% endfigure %}
