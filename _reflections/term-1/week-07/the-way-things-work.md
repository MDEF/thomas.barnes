---
title: 07. The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
icon: 'images/way_things_work-07.svg'
published: True
---
**12 - 16 November 2018**


This week we delved into the world of electronics, sensors, systems and networks. We looked into the electronic devices that surround us and the connectivity that has transformed how we live in the last few decades.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/Printer_pieces.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/Printer_hack.jpg"/>
  </div>
</div>
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/phone_PCB.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/phone_examine.jpg"/>
  </div>
</div>

**A Wasted Product**


Our first task was to take apart products and see how they work. We dismantled phones, printers and coffee makers.  I undertook a similar task to this back in 2007 as part of my Industrial Design BA at Loughborough University in the UK. Back then the discussions were centred heavily on electronic waste. The increasing amount of it, and how difficult it was to fix or disassemble a lot of the manufactured items, which unfortunately has only got worse.

There have been systems put in place like the EU WEEE directive to deal with waste produced by electronic products. It has been in place since 2003, and stipulates that all the electronic waste should be processed properly and hazardous materials recovered safely. It’s aim being to prevent electronic items which contain both valuable and toxic materials being put in the ground or burnt. This system is undoubtedly very important even if there are issues surrounding its implementation. However what it doesn’t do is address the forces which are causing such an increase in waste in the first place. Issues such as consumerism, planned obsolescence and a throw away culture contribute to mountains of E-waste. This is particularly a problem in developing countries which these products get shipped to. There is a huge amount of human suffering caused by the effects of trying to process all the waste we produce.

{% figure caption: "*Bridges made of E-Waste in Ghana*" %}
![]({{site.baseurl}}/trash_bridge.jpeg)
{% endfigure %}

{% figure caption: "*Scultpure made from E-Waste when the WEEE directive began in 2003*" %}
 <div class ='center-image'>
    <img src="{{site.baseurl}}/Weee_man.jpg"/>
  </div>
{% endfigure %}

There are people trying to change these patterns however. [Fairphone](https://www.fairphone.com/en/) has produced a device which is modular, enabling parts to be repaired or replaced. Leading to longer lifespan of the handsets. They also use open source software, so users are not reliant on companies which can affect their devices and control their functionality. Movements are also appearing with the aim of teaching people how to repair their devices, rather than just throwing them away. One of this is [Edinburgh Makery](https://www.edinburghremakery.org.uk). Their vision being to revive a repair culture. This however can only work if products are made easier to get into and repair. Companies like apple are making it harder to do, forcing people to replace broken products rather than fix them.

Greenpeace has produced a rating system to show how companies are performing when it comes to sustainable electronics: [A Guide to Greener Electronics](https://www.greenpeace.org/archive-international/en/campaigns/detox/electronics/Guide-to-Greener-Electronics/). I'd be interested to kow impact of these guides, and their limits. They need to get wide engagement and need to be part of the public domain and discourse. Currently I feel only people who are already interested in these issues will be the ones who will engage with it, the real challenge is getting more people to care, and only then can behaviours be changed.

{% figure caption: "*Greenpeace Electronic Guiide*" %}
![]({{site.baseurl}}/Green_electronics.jpg)
{% endfigure %}

**Studio Mood Indicator**

As part of this weeks work we created our own projects in teams using Arduinos. All the projects would be connected together at the end of the week, and we had the choice of creating an Input or Output. We chose to create an Input, in the form of a mood indicator for our studio. We wanted to tell if the feeling in the room was relaxed, focussed or excited.

To do this we wanted to tell how many people were in the room, and compare this to the noise and light levels. To do this we used Light Resistant Diodes (LDR) and a microphone. The LDR measured changes in the levels of light as people moved through a doorway and entered the room. It had been our intention to use an ultrasonic sensor, and use it as counter. This proved problematic in practice, getting the code and sensor to work properly in the timescale. Also when we moved the sensor to the wifi board it wasn’t powerful enough (3.3V not 5V). We switched to an infrared sensor, but that also gave us such strange values that we finally stuck to the LDR. This iterative process showing us the reality of moving from an idea that seem straight forward in theory, to being more of a challenge when being tried out in the real world.

{% figure caption: "*Testing out the ultrasonic sensor*" %}
![]({{site.baseurl}}/ultra_test.gif)
{% endfigure %}

We had to attribute the different levels from the sensors to moods or ‘states’ in the room.
<p align="center">
People + light + noise = exited<br>
People + light – noise = focused<br>
People - noise - light = relaxed
</p>

This is the same to applying labels to an AI system. We were trying to transfer physical states in the room into an measure of feeling. However even at the start we were struggling to define the states as we each saw them slightly differently. They could also be applied in different ways. This shows how the data in these systems is only one aspect. The key factor is how we interpret it. We had only a few variables, and due to time constraints created an oversimplified system. Which didn’t accurately measure the different contexts required when interpreting something as complex as mood.

{% figure caption: "*Light and sound sensor board*" %}
![]({{site.baseurl}}/lightsound_board.JPG)
{% endfigure %}

{% figure caption: "*Data from our sensors being sent to the raspberry Pi*" %}
![]({{site.baseurl}}/Data_input.JPG)
{% endfigure %}

**Input and Output Mix**

As mentioned before the aim was to have different inputs and outputs, and link them all together.

Examples of Inputs:

- Room Mood Indicator using sound, light and people entering the door<br>
- Plant capacitive sensor

Examples of Outputs:

- Balloon inflating and exploding<br>
- Light wave machine<br>
- Tone changing phone

{% figure caption: "*Shock as the plant capacitive sensor activates the phone*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/Touch_plant.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/Shock_plant.JPG"/>
  </div>
{% endfigure %}

The door sensor caused the balloon to explode, and the by touching the plant the phone changed its tonal output. These inputs and outputs were all connected together using ‘Node Red’. Where you can control the incoming data, mix it how you want to control the outputs and how they operated. Outputs could even become inputs, flowing back into the system triggering something else. It can gain control of different variables, and use them to construct results that are desirable for a given situation.

![]({{site.baseurl}}/node_red1.png)

![]({{site.baseurl}}/Node_red2_speedo.png)


**Freedom to change and adapt**

I’ve already touched on the current trend to restrict repair and adaption, control of products and black boxing. This doesn’t just hold true for physical products. This is the same for software and operating systems. An example of a system that doesn’t hold itself to these limits is Linux. I didn’t know much about Linux before this week, I knew of it but didn’t really understand the principles on which it was created.

Linux works under the copyleft licence. This is the opposite of copyright, where the main idea behind it is that creators restrict what others can or cannot do with their works and must grant individual permission to do otherwise. Copyleft however aims to increase freedoms to users.  The core concept of copyleft is that users should have the right to freely use, copy, modify, and distribute works however they want, with one crucial clause: all derivative works must offer the same freedoms to users.

- Freedom 0 – the freedom to use the work,
- Freedom 1 – the freedom to study the work,
- Freedom 2 – the freedom to copy and share the work with others,
- Freedom 3 – the freedom to modify the work, and the freedom to distribute modified and therefore derivative works.

This system doesn’t allow people to do anything, and you you need to give other people the same opportunities to change your work, as you have done to someone else’s.

As a result of this system, Linux has been able to develop, improve and change in a very open way. It allows for collective adaptation and progression, rather than holding onto developments to increase private gain.

**Reflections**

People have more access to technology, programming and networking than ever before. This means the opportunity to innovate, explore and play is becoming much easier and cheaper. We can access other peoples work and modify it across global platforms. However I have a concern with it is at the moment is it still in the gimmick stage. We ourselves created some amusing and fun projects, and in the interests of education increased our knowledge in an engaging manner. However there are countless examples of projects that are completely pointless, using cheap electronic components from China. If we don’t move rapidly into meaningful uses, then we are just contributing to more electronic waste filling up our landfills and oceans.

That slightly negative point aside, there is a great opportunity to use these technologies in positive ways. A strong one I’d like to investigate is using them to create interactive installations for my project. I think they will be vital in either gathering data or portraying it as a message about the issues I am looking into. Also we can increase people’s education surrounding re-use, repair and recycling. Giving them the tools to retain materials and products, then pressure will be put on designers and manufactures to make products that reflect those ideals.

**References**

[Reduce, reuse, reboot: why electronic recycling must up its game, 2017](https://www.theguardian.com/environment/2017/nov/20/electronic-recycling-e-waste-2017-gadgets)
