---
title: 06. Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
icon: 'images/ex_ai_06.svg'
term: 1
published: True
---
**5 - 9 November 2018**

![]({{site.baseurl}}/images/AIFlowChart.png)

This week we looked at Artificial Intelligence. We learnt about the automated systems that are getting ever more present in our daily lives. We designed ‘intelligent objects’ and began to discover some of the challenges and opportunities that surround them. An interesting part for me was the ethical implications and impact that AI can have. This is an important area, because it is becoming so ubiquitous, so it requires us all to engage with it’s significance and all decide how we want AI to be used.

This week confirmed my belief that a technology in itself can’t be in itself good or bad, or given a moral label. It is dependant entirely on the context is designed for and how it is deployed in the real world. Ideas around AI has been around since the 1950s, and there have been many stories written about the positives and negative sides of it.  

I was reminded this week of a cartoon called Futurama, which was written in the late 1990’s. There is a character in one episode called Robot Santa. He was designed to decide who had been nice and who had been naughty, and give presents to nice people. His thresholds were set far too high however and he decided everyone was naughty and delivered a festive retribution to a terrified New York every Xmas. On one level this is just a silly cartoon, and the story is ridiculous. However it is funny, and touches on some of the themes we discussed this week: Defining intelligence, inputting social constructs to a machine, safety in systems, and their robustness.

<br>


<iframe src="https://giphy.com/embed/rPa4hn4vkwnw4"  width="700" height="360" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/christmas-futurama-robot-rPa4hn4vkwnw4"></a></p>

<br>
**What is Intelligence??**

We started the week by exploring the whole notion of intelligence, and we came up with various topics that covered ideas surrounding it.

Logic, understanding, self-awareness, learning, emotional knowledge, reasoning, planning, creativity, problem solving, decision making. The act of retaining knowledge and experience and applying it in various applications based on the environment or context.

We can certainly create machines that are good at decision making, that can learn and problem solve. However we are not quite at the stage of creating a machine that is self aware. A machine does not understand the world as we do, and they are still very limited to the constraints of their programming. This can be seen in John Searle’s ‘Chinese Room Thought Experiment’. A machine can be programmed to write things in the Chinese language, just as a human can be given instructions on how to do the same thing. However the neither will actually understand what they are doing, they are just following a set of rules. There is no grounding or comprehension, and so the machine can’t be seen as ‘thinking’ as we do it.

**Activities are learned not programmed**

Ai’s ability to learn from it’s mistakes and improve does make it very different from purely computational actions. This gives us the opportunity to witness different ways of behaviour. One example of this is when Facebook chat bots started communicating with themselves in 2017. They created there own shorthand language which helped them negotiate the best deals. This happened in part due to their goals having no reward being given for talking in English. They therefore didn’t have to limit themselves to making sense in that particular form of language. This does highlight the unforeseen consequences when trying to program these systems.

![]({{site.baseurl}}/neural_network_brain.png)


**Ethics and responsibility**

The ethics surrounding AI’s use and impact are massively complex. Machine’s are great following tasks in clearly defined fields, however the nuances of social, cultural and environmental structures are very challenging to program. An ai system in itself won’t be biased, however if the information being inputted is skewed in one direction or the other it will affect all the outputs. The impacts of these systems are already being felt around the world.

How much people trust system in the future will be an indicator of their power. Some people argue there is already a ’broad over-acceptance of algorithmic authority’. Where there is little critical analysis of either the activities that these systems are undertaking or the results being produced. This can be seen with the [Cambridge Analytica Case](https://www.theguardian.com/news/series/cambridge-analytica-files) where huge amounts of data were harvested and used to target people politically.

[The partnership on Ai](https://www.partnershiponai.org) was put together to specifically target some of these issues. It is formed of some large tech players like google, facebook, apple, IBM and many more.

Here are some of their aims:

- Develop best practice
- Advance Public understanding
- Provide an open and inclusive platform for discussion & engagement
- Identify and foster aspirational efforts in AI for socially beneficial purposes

I am personally sceptical that these aims are really true. I feel it is just a bit of a PR stunt, when AI still has a lot of nervousness around it, especially in the public domain. I think the aims are all required, however are the likes of google and facebook best placed to put societies interests over their own? They don’t have a great track record of tackling behaviours which have fed to some very difficult situations.

There needs to be transparency for all parties in a system. Where everyone can access the system and see why decisions are being made. If a system is put in black boxes and hidden away then it will firstly be difficult to trouble shoot when things go wrong. It will also build distrust, particularly if decisions are being made that effect people negatively.

**Inputting Data - Translating the physical world into ones and zeros**

Ai systems by the nature are digital, whereas our brains work in a much more analogue way. This means we somehow have to transfer the physical world, and often our interpretation of it into a binary model. If we all agree what ‘red’ is then the label is relatively straightforward. However social constructs like ‘greed’, and emotional states like ‘happiness’ are far more challenging. Particularly when they are so subjective. The way things are labelled matters. This can be seen in the symbol grounding problem - How to get an objective meaning when something is described perspectively? I think this makes it very challenging to create systems which assess or reflect human behaviour. We don’t always find it easy to define what ‘happiness’ or ‘love’ is when referring to ourselves. How can we then transfer it into a machine, which has to have it transferred into a range of clearly defined states?

**[Autodraw](https://www.autodraw.com) - does is increase creativity or decrease it?**

In this application people can start drawing something, and the program will recognise it. It will then suggest using a range of similar but high quality drawings in its database that match the sketch. This means people can quickly produce high quality work, from just a doodle. However is this in a sense is just a more advanced clipart? People don’t have to put effort into make something. Mistakes and the genuine nature of a drawing provide a strong connection to work. The opposite is everyone has really polished work, of a ‘high quality’, but that it all looks the same. Everyone has to express themselves in the same way. Who is defining what this ‘quality’ looks like? Technical skill level shouldn’t be the only factor in having a good piece of work. However I’m also aware that not everyone is confident in drawing or expressing themselves visually. This could support people in creating visuals that they might be happier sharing with others.

**Reflections**

I think the knowledge and context of this week is very important for many projects I could undertake in the future. This doesn’t necessarily mean I will be using machine learning or neural networks directly (although maybe I will). However an understanding of how the agents in current and future systems behave and interact is important. We need to know how systems get their information, how they process it and most importantly what the impacts might be. Then have a system flexible enough to react to feedback from it’s actions.

Designing systems that learn from interaction with people is an interesting area. Maybe then they will be able to get a context on why people behave in a certain way, not just what they do or how they do it. One possible avenue to look in in relation to my project would be trying to train a system that best achieves a sustainable outcome for a project. However not considering just material wastage, but motivational factors. There is often an split between what people want to do, and what they are able to do in their current situation. This could then provide directions that limit impact. By being able to input more of the factors into a system, could it be easier to see what are the limiting ones and try and intervene?

Possible data sets:

Materials and access to them<br>
Economic freedoms<br>
Surrounding systems - eg. Waste processes<br>
Interest in sustainability<br>
Time constraints<br>
Incentivising sustainability.

**Links**
- [Inside Deep Dreams - How Google Made it's computers go crazy](https://www.wired.com/2015/12/inside-deep-dreams-how-google-made-its-computers-go-crazy/)
- [Artificial Creativity: If a machine creates an image that looks like art, can we call that creativity?](https://www.linkedin.com/pulse/artificial-creativity-machine-creates-image-looks-like-simon-hudson)
- [Art in the Age of Machine Intelligence](https://medium.com/artists-and-machine-intelligence/what-is-ami-ccd936394a83)


**References**

- [What We Get Wrong About Artificial Intelligence, 2016](https://www.iotforall.com/human-computer-interaction-design-ai/)
- Artificial Intelligence Driven Design by Joel Van Bodegraven.
