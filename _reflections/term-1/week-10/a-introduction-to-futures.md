---
title: 10. An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
icon: 'images/intro-future-11.svg'
term: 1
published: true
---

This week we had look at futures, and tools to help explore, envision, and build possible future scenarios, lead by Elisabet Roselló. We looked into the of history future thinking and how it has and hasn’t changed over the past 100 years or so. The future isn’t fixed, it hasn’t happened yet, however by looking forward we can reflect and gain insight on decisions we make in the present.

**Archaeology of the Future**

There are many images of ‘The Future’ which we all know from popular culture and science fiction. Cities full of sky scrapers, flying cars and great organic road networks. What I didn’t really consider before now is that we have been using the same images for the last 100 years or so. We are still focussed on progress through technology, and cities that are based on the paradigms of the last industrial revolution still hold true. This is despite the changes in society, how we communicate, consume information and connect to each other.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/old-new-city.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/chicago.jpg"/>
  </div>
</div>
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/bubblecar.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/space_city.jpg"/>
  </div>
</div>

The mental structure of time is complicated, plus it depends a lot on where you come from. If you are from a western culture it is easy to think of it in a linear way. We look at what has happened in the past, live the present and envision options for the future. However if you are from a different culture you will visualise and experience time differently.

*‘Time seems to come at us head-on: the future in front, the past behind. Not so for the Aymara people of the Andes. Because the past is what they have experienced, it lies ahead, where they can see it. The future remains hidden, so it is behind them’*

The structures society has built to describe time are also built on repetition, hours, days, months, years. As certain aspects of our lives repeat, we think we know what is going to happen. There is a conceit that we are in control, somehow we know what will happen in the future. One key thing we learnt from this week is that we are not here to make predictions. We cannot know what will happen, we can however create scenarios that have different values of being probable, plausible and preferable.

**Scenario Building**

We undertook an exercise in scenario building, using a canvas that Elisabet supplied us. We were asked to envision a world which were based on a combination of two scenarios.

**Decolonial Axis** - After centuries of imposition or pressure to adopt culture, languages, policies and economic models of some specific foreign nations, some territories react to it, or have an urgent need of political problems and issues. It tends to reconnect with local cultures and singularities to manage their own narrative, as well as their own future, with their own idiosyncrasies, symbols, answers and  decisions. #afrofuturism #ethnofuturism #gulf_futurism

**Bizarro Axis** - This is about a moment in time in which normalises have been ‘broken’: What was normal yesterday, today is not. But in this plot, moreover no familiar normality is established. Everything is really, really strange, surprising, bizarre, hilarious, terrifying psychedelic, surreal. With this axis, everything is allowed except the obvious and familiar. #alien_world #new_normal #postgender #postidentities #huge_wtf

![]({{site.baseurl}}/canvas.jpg)

Description of world:

People in this world are 100% self sufficient, so there is no need to rely on each other so the usual social skills have become irrelevant. People now rely on synthetic emotions, pills which regulate how they interact with other people.
There is no government because everyone has everything they need, so they self regulate as well. There are no hierarchies because when people have what they require it becomes a leveller and flattens positions in society. Modern taboos like killing, stealing and shame, based on westerns religious morals are removed.
People start however to lose their identities, and also sometimes gain multiple ones. There is the ability to connect to one’s roots through the same synthetic emotion techniques. Learning about old cultures, systems and traditions. This however causes friction, as old values conflict with new ones.

There was also an activity of backcasting - describing the stages needed for which this situation could occur:

From Present:

Countries isolate, protectionism continues and crashes the global economy
People react and open borders, this continues and greater areas widen to allow free movement of everyone. Simultaneously a technology is developed to allow people to support themselves and become self sufficient. Past geographic importance becomes obsolete.
As people have continue to mix it becomes harder to govern, they become more self reliant and government is abolished. Less conflicts as people have what they need.
People start losing identity and synthetic histories are created, to connect people to their past, along with synthetic emotions which become a recreational drug for some.

As a group we pushed the bizarre scenario as far as we could, while blending the decolonial attributes to a small extent. Relying on fictional ‘technologies’ to support a seemingly absurd set of actions. However as seen by the Jim Dators Laws of futures these absurd ideas aren’t always to be dismissed to readily:

‘Any useful idea about the futures should appear to be ridiculous.
Because new technologies permit new behaviours and values, challenging old beliefs and values which are based on prior technologies, much that will be characteristic of the futures is initially novel and challenging. It typically seems at first obscene, impossible, stupid, "science fiction", ridiculous. And then it becomes familiar and eventually “normal.”'

**Science Fiction**

I’ve mentioned in some of my past posts about my interest in science fiction. When done well it is an amazing form of scenario building. Where whole worlds are created, and there is scope for the big topics to be explored. Whether it be dystopian futures, based on reflections of current trends. Or more speculative works which are based on future technologies that don’t exist yet, there is often correlation in techniques being explored on this course. I’ve listed some books and authors that I’ve read or want to read more of after this week’s work.

1984 - George Orwell<br>
Dune - Frank Herbert<br>
Amatka - Karen Tidbeck<br>
The year of the flood - Margaret Atwood<br>

<br>

<div class='four-image'>
  <div class ='column-four'>
    <img src="{{site.baseurl}}/1984.jpg"/>
  </div>
  <div class ='column-four'>
    <img src="{{site.baseurl}}/dune.jpg"/>
  </div>
  <div class ='column-four'>
    <img src="{{site.baseurl}}/amatka.jpg"/>
  </div>
  <div class ='column-four'>
    <img src="{{site.baseurl}}/flood.jpg"/>
  </div>
</div>

**Key terms and take homes from this week**

- No Future - Dystopias
- Posthumanist
- Transhumanist
- Transition Design
- Post normal times
- Black swans
- The unthought

**Exercise for my project**

Scenario:

*Fabs labs have easily accessible knowledge about material choices which include innovative, accessible and non-toxic options. They have engaging learning methods to teach Fablab users about the options, contexts, benefits and limits of various materials.
Incentives are given for re-using materials and a clear system is in place to trace materials as they flow through the lab. No materials ends up in landfill, as all materials that can’t be re-used in the in-house have other uses for external Fablab associates and organisations.
Toxic materials aren’t banned outright, but they are discouraged, strictly controlled and kept separate from non-toxic options.
Space, time and value is given to processing materials, as an important part in the design and making process.*

Back casting - how to get to this scenario:

- Fablabs realise that there is a need to become more sustainable, or to at least know what their impact is. They initialise programs to measure what is happening. Track materials, measure waste, and energy consumption.
- There is also a study of the impact of short and long term exposure to chemicals and toxic materials in the lab. Steps are taken to phase out harmful materials, replacing them with less toxic options. Where this can’t be done control measures are increased.
- Once data on material use, energy use and environmental impact starts to build. Comparisons to other manufacturing techniques are possible, comparing impacts of similar products made by conventional mass production techniques.
- Educational programs like Fab Academy begin to educate users on best use practices, and develop engaging platforms to increase material literacy and designing for circularity.
- Fablabs develop new ways to promote values of self sufficiency, re-use and reclaiming materials. Setting aside space, resources and time to allow a circular approach to the majority of their processes.

**Reflections**

This is a first attempt of envisioning a scenario where fablabs are actually quite different from where I think they are currently. However I think i need to push for more, the scenario and steps seem quite pedestrian in their approach. They need to be plausible yes, but I think new and radical approaches need to be taken to speed up the processes. This could take a while to implement, as issues relating to resistance to change are tackled.

I will undertake the process again, trying out slightly more odd scenarios to see if certain considerations that I haven’t thought of before fit. Measures like this are suggested all the time, and often get ignored or dismissed. However issues relating to sustainability are filtering into peoples consciousness, so capitalising on traction in this area is vital.

References:
- [What time looks like to different cultures](https://www.popsci.com/what-time-looks-like-to-different-cultures#page-2)
- [Future Studies - Jim Dator](http://www.futures.hawaii.edu/publications/futures-studies/WhatFSis1995.pdf)
