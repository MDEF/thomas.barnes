---
title: 05. Navigating the Uncertainty
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
icon: 'images/nav-world-05.svg'
published: True
---
**29 October - 2 November 2018**

This week we looked into living in a world with so many challenges and uncertain futures. Where issues like climate change appear to proceed unchecked and ignored. How do we as designers hope to tackle such large problems and remain positive?

We had talks and discussions with Jose Luis de Vincente, Pau Alsina, Mariana Quintero, Toma Diez and Osar Tomico. I have explored some of the themes that were mentioned and resonated with me.

**Envisioning the future**

Frameworks for being optimistic in an uncertain world: There is an common doom reflex which kicks in when discussing some of these challenges. Particularly when it comes to discussing big topics like climate change. While it is necessary to be aware of the situation, if we get too pessimistic then it restricts our ability to deal with any of the issues. In his TedTalk [How to transform apocalypse fatigue into action on global warming](https://www.youtube.com/watch?v=F5h6ynoq8uM) Per Espen Stoknes outlines his vision of how we can move from apathy to positive action.

<p align="center">
Distance - Social <br>
Doom - Supportive <br>
Dissonance - Simple (actions)<br>
Denial - Signal (progress)<br>
Identity - Story<br>
</p>

This framework requires an understanding of the problem and then outlines ways each one can be addressed. I’m almost more interested in his process than the result. The way of breaking down the larger problem into understandable pieces. Which allows a way to flip them into more positive behaviours. Could a similar process be applied to the issues affecting my area of my work?

**Design Fictions**

One thing that came evident from Jose Louis’ seminar was the need to change the current way we operate, on many different levels e.g political, social, cultural etc. These could be big, crazy, systemic changes that challenge how we live today. This is inherently difficult due to the complexity of the situations we are looking at. One option that was mentioned was using Design Fiction as a tool to envision possible futures.

 I looked into design fiction as a way of creating speculative scenarios. This was also in part linked to my personal interest in Science Fiction, and the ability to create fully realised worlds. The interesting elements of design fiction don’t just deal entertainment or escapism, they provoke reflective activities. Engaging with 'laws, ethics, political systems, social beliefs, values, fears, and hopes, and how these can be translated into material expressions’. This can lead to scenarios which are removed from a lot of the constraints that curtail imagination. This can also have the result of reducing attention given to ‘commercial applications and redirecting it toward more social ends’.

This is not to say that a design fiction is to be completely removed from reality. However in terms of envisioning a future, it can stimulate a process of exploring the many facets which a possible future will explore. I liked this quote in terms of describing its position:

 *‘Design fiction works in the space between the arrogance of science fact, and the seriously playful imaginary of science fiction, making things that are both real and fake, but aware of the irony of the muddle’*

**How we see the world - layer of meaning - metaphors**

We discussed the term read-write technologies, which relates to the exchange of information. This can idea can be explored in many ways and really does affect how we see the world and the objects inside it. An example given being that a chair is not just a chair, but a reflection of the social constructs which inform it as an item. For instance it is in addition to being a place to sit, a way of controlling a classroom of people and influencing their behaviour. We are all subject to this conditioning all the time. Being ‘nudged’ to behave in certain ways, often without considering it at the time.

How we perceive the world used to be limited to the sensors we possessed, otherwise known as senses. However we have the ability to imagine that there is more than what we can see / smell / touch etc. As a result we have invented a myriad of tools that enable us to access many more elements of our reality than we can on our own. e.g. hearing bats using echolocation that use frequencies too high for us to hear. We are constantly changing our limit of information exchange, so what does that mean in terms of how we see ourselves. Even in terms of our notion of being ‘human’. A point that reflects this is that we contain a vast amount of micro-organisms, which we need to function but don’t identify in genetic terms as being human. We end up being made up of more non-human cells than human. As a result could we describe ourselves as a ‘a symbiotic multiplicity’? Our current systems in society don’t reflect even the symbiosis that allow each of us to physically function.

<p align="center">
Metaphors - Symbols - Rituals <br>
Possibility - Knowing - Perception <br>
Interface - Information - Technology
</p>

This idea of of how we see ourselves will continue to be a massive issue as current technologies advance in the coming years. The ability for AI and algorithms to ‘know’ people is already having a strong impact. Social constructs can be manipulated by targeted messages of ideology or political ideas. If ‘governments and corporations succeed in hacking the human animal’ then they can control behaviours with ever more accuracy. The information interfaces become more complex, and issues such as privacy, transparency and responsibility become more important.

**Systems**

As part of my work looking into reducing material waste I need to look into the current systems that operate. The overall aim being to find points of intervention where a ‘small shift in one thing can produce big changes’. This is explore in lots of detail in Donella Meadows Leverage Points: Places to Intervene in a System. The notion of changing the parameters in a system is very quick and easy, but has little long term affect because it doesn’t really change behaviour. Conversely being able to change the paradigms of systems is massively powerful but very hard to achieve e.g. changes in society - traditions and economic models. I’m not going to be able to easily affect large complex systems e.g. ones that govern a city. However I can look at systems in a smaller context. This doesn’t mean that affecting change can get easier, because barriers and influences may be the similar. However it is may be possible to see the different threads on a smaller scale, and be able to pinpoint where to make an intervention.

![]({{site.baseurl}}/leverage_points.svg)

When looking at systems it can be necessary to change the view where a ‘person understands themselves at the centre of the world.’ It’s not very helpful in to take a central view in a system as we are only tiny actors inside one. It is more important to been see ourselves as a ‘participant’. We can then let ourselves be affected, adjust the flow, and influence rather than try and block and obstruct. Another element to consider is not focussing so much on the physical parameters, but creating scenarios to tackle abstract concepts and drivers like ‘desires and fears’. This then allows us to “build contexts that address them.’ It requires moving a step backward and dealing with the root causes of behaviour, rather than just trying to fix the consequences.

**Case Studies**

Superflux: Translating future uncertainty into present day choices

Superflux is a design studio which uses speculative design to provoke and inspire engagement with the uncertainties of this rapidly changing world. There is no fixed future, and they use the freedom of possibility rather than the constraint of determinism. By examining options in the future we can better inform the decisions we make in the present.

“One of the things we find in our work that is really challenging for people to do, is to move from a place of feeling safe, and certain, to being able to embrace uncertainty, and not freak out about it”

Example project: Stark Choices

<br>
<iframe src="https://player.vimeo.com/video/297612827" width="640" height="360" frameborder="0" allowfullscreen></iframe>
<br>


Superflux created a very immersive simulation examine the challenges of automation and what a future world or work could look like. At the moment the discourse is very skewed towards negative scenarios. This project aims to provoke people into considering what are the other opportunities available. Automation doesn’t necessarily mean no work in the future. However it requires looking at different employment models, economic interests, inclusive skill adaption. Making sure everyone has a stake and the wealth and opportunity doesn’t remain in the top 1%.

The consideration I’d like to understand more is the scope and impact of these projects. Personally I really like the way they challenge people’s perceptions, and stimulate more powerful reactions. However are they accessing people already interested in what ever topic being investigated? How do they reach the people who don’t understand, or don’t care?

Precious Plastics

Precious plastics is a global community which aims to reduced plastic pollution. It does this through a simple of idea. What do I need to re-use plastics? Tools and machines were created to shred, extrude, inject and compress plastics. One thing that makes this project special is that Dave Hakkens (project founder) put all his designs online and made them open-source. He wanted other people to make his designs and do it themselves. As a result there is now a global network of people working on the designs, improving them and sharing knowledge.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/2KlW_WmV3Bw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>

Precious plastics a great example of what happens when knowledge is shared, is open and free. The organisation doesn’t earn money from the work it does. Which is an issue and they rely on donations to support their system and work. However with crowd funding as an option, this means projects can remain independent and not be influenced by outside investors, backers or sponsors.


Other cool projects by the great Dave Hakkens <br>
[Phone bloks](https://phonebloks.com) <br>
[Story-hopper](https://story-hopper.com) <br>
[Project Kamp](https://projectkamp.com)

**Reflections**

This week felt like a bit of a turning point. Just in terms of how I’m starting to approach my work. Rather than just documenting everything that we did, I thought it was better to start picking out the relevant bits for me and my work. I enjoyed going into more depth and pursuing areas that I felt a connection to.

I feel for me an interesting aspect is how we blend ideology and reality. It is all very good having a vision of a world or system that behaves differently. The challenge is always what are barriers in place when you apply it to the current reality, and what are starting steps required to move towards a required vision. That's the aim of this course though, to make the steps which move us in a direction we want to go in.

Some themes to take away: Have a vision. Don’t get overwhelmed. Break down issues. Remain positive. Follow interests. Share with others.

**Reading list**

- 7 cheap things - Jason W. Moore and Raj Patel
- Life 3.0 - Max Tegmark
- Lessons for the 21st century - Yuval Noah Harari
- A sideways look at time - Jay Griffiths
- A modern man in the making - Otto Neurath
- Expulsions - Saskia Sassen

**References**

- [Dunne, A. and Raby, F. (2014) Speculative everything: Design, fiction, and social dreaming. Cambridge: The MIT Press.](https://mycourses.aalto.fi/pluginfile.php/40001/course/section/15792/Dunne_Raby_Methodological_Physical.pdf)
- [Design as Participation by Kevin Slavin, 2016](https://jods.mitpress.mit.edu/pub/design-as-participation)
- [A short essay on design, science, fact and fiction. Julian Bleeker March 2009](http://drbfw5wfjlxon.cloudfront.net/writing/DesignFiction_WebEdition.pdf)
- [Design fiction - Davis Levine, 2016 ](https://medium.com/digital-experience-design/design-fiction-32094e035cd7)
- [We have never been human by Sean Miller, 2016](https://www.popmatters.com/we-have-never-been-human-2495438275.html)
- [Leverage Points and places to intervene in a system by Donella Meadows ](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/)
