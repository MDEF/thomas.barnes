---
title: 11. From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-10 12:00:00
icon: 'images/bits-atoms-12.svg'
term: 1
published: true
---

This week we combined the design skills we learnt in WK 3 (design for the real digital world) with the electronics and programming from WK 7 (the way things work). It was a bit of a pre-course for fab academy. Getting used to the facilities and the activities we will be undertaking next year. Our main task was to make a machine, and preferably one which didn’t blow up!


<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/scribble1.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/pollock1.jpeg"/>
  </div>
</div>

**Pollock Bot**

Our group’s main aim was to make a drawing machine. We got very excited about all the different options, as there are lots of different existing designs to do this already. The hardest bit definitely actually deciding on what idea to go with. We initially wanted to try and make a few drawing bots, so we called the project animal farm. However this quickly got scaled down, as making one was hard enough. The machine got renamed to Pollock Bot after the designs it created reminded us of the artwork by Jackson Pollock.

To see the project in more detail see the project page we created: [https://mdef.gitlab.io/animal-farm/](https://mdef.gitlab.io/animal-farm/)

{% figure caption: "*Artist or Robot? - Artwork by Jackson Pollock*" %}
![]({{site.baseurl}}/Jackson-Pollock.jpg)
{% endfigure %}

**CAD skills**

I designed the body of Pollock Bot using Fusion360. I’ve have used similar programs in the past but not for a while. I found that although I know how use the software I’m quite slow and don’t know the most efficient modelling practices to do things properly. I definitely need to practice more to be able to CAD projects quickly.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/plan-views-p.png"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/top-pollocks.png"/>
  </div>
</div>
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/front-pollock.png"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/rear-pollock.png"/>
  </div>
</div>

**Remember the wires…..or just solder**

We tried to make sure we had enough space in the bot by modelling all the components that go on the inside. We didn’t quite take into account all the cables though, and in the end not everything could fit inside. We also had a lot of trouble with loose connections when the whole thing vibrated around. In the future planning where all the components go inside will be a priority. The other alternative is to solder the components to boards, but we didn’t have the time / expertise to do that this week.

**Electronics and Programming**

I have lots of experience using some of the equipment in the Fab Lab, including the laser cutters, CNC machines and 3D printers. My area of weakness is definitely in the area of electronics and programming. I helped a bit with the LED lighting of Pollocks pen. However, it is something i'm still not very confident in, and would really like to get better at. The ability to be able to include electronics and sensors in projects really increases the scope of what i could do. 

**Material use**

I can’t do a week’s documentation without mentioning material use and waste. Now I’m well aware of the iterative nature of prototyping. I’m also aware that the fablab and IAAC is a learning environment. This means that materials will get used in the process. They will also have a short lifespan as prototypes are moved on and previous modes outdated. The good thing is that most of the quick models were made of cardboard, however there was still quite a bit of plywood used. The projects had a lifespan of one day. Then people disassembled them and took back the internal components. Are the material and energy used in this week justified by the learning and knowledge gained?? I hope so. Otherwise we are just contributing to a problem under the guise of learning how to improve it in the future.

 **Fab lab and my Project**

I will have to use the Fab lab a lot next year as it is the focus of my project. Looking at issues of sustainability and how the issues around material use are tackled. I will be questioning some of the very essences of how the space works, and focusing on some of the core challenges i think they struggle with. I will try and use Fab academy to design and make items that can help test out ideas, bring awareness to issues or gather data.
