---
layout: page
title: Material Driven Design
permalink: /material driven design/
---

In this module we are looking in detail at materials. At the connection we have lost with materials we work and live with. How the effects of the materials we use are changing the world dramatically, and what we can do to shift the relationship we have with materials in design and digital technologies.

This module aligns completely with the my area of interest, and the work i will be pursuing as part of my major project. Increasing material literacy, and how to connect the digital technologies with analog materials in a more symbiotic way.

## Wood Carving in Vallduara

Friday 25th January

We all went up to Vallduara and spent the day wood carving. Not only was this a very enjoyable way to spend the day in the sun, but it also was a great practical way to experience some of the concepts that had been discussed the day before in class.

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/walk_woods.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/carve_stick.jpg"/>
  </div>
</div>
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/vhouse.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/spoon.jpg"/>
  </div>
</div>

![]({{site.baseurl}}/term-2/material/all_spoons.jpg)

We selected pieces of wood, and using wood carving knives and axes, we spent the afternoon making spoons.

Reflections:

- I chose a piece of almond wood, mainly because i like almonds, and also i didn't know the wood, I was told it might be a bit hard but i stubbornly thought it would be ok. It was hard, and my thumbs actually blistered in the process of using the knives. I only really learnt this however by actually going through the experience of making the spoon.

- The tools make a big difference. Like with any practical activity, the tools you use really change the experience. I was struggling with the knives, but the axe made it much easier to remove larger amounts of material.

- The process becomes quite meditative. There was a focus or zone which i reached. Where i just wanted to continue and get my spoon. I didn't want to stop and eat, i just wanted to complete the task. This feels very different from a lot of the computer based tasks, like creating CAD models. Where I feel drained by a long task in front of a screen, rather than excited and energised. However this could partly be the novelty, and the location which was fantastic.   

- Connection to the end product. Many people were really proud of the work, even if the spoons were unfinished, wonky or wouldn't actually work.

- The topic of online and offline was discussed, where we went from being very aware of every action, to it then becoming a bit more automatic. How familiar one is with the tools you use is an interesting one. The interface between humans and digital tools is very different to more non-digital techniques.

## The futures bright the futures orange

We now need to doing material exploration of our own. Finding a raw material to work with, which is fully biodegradable.

My chosen material: **Orange Peel**

![]({{site.baseurl}}/term-2/material/oranges1.jpg)

Reasoning:

- We need a raw material that is readily available. There are many many orange juicing places in Barcelona, in many supermarkets and juice bars. The peel and pulp often just goes into the bins. I have asked and i can just take a bag that would otherwise be thrown away.

- Oranges have a great colour, smell and are associated with health.

- Orange peel can even be used as artwork.

{% figure caption: "*[Yoshihiro Okada orange peel artwork](https://www.atlasobscura.com/articles/orange-peel-art-japan)*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/shrimp_peel.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/bird_peel.jpg"/>
  </div>
</div>
{% endfigure %}

**Chemical Composition of Orange Peel**

- Citrus peel is an important source of essential oils, especially limonoids (Espiard 2002) which are used for the production of perfumes and other cosmetics. Limonoids have also different biological properties (antiviral, antifungal and antibacterial activities) (Ma et al. 2009).

- Citrus peel can be used for the production of products like biogas, ethanol, or volatile flavouring compounds (Djilas et al. 2009) or used for extraction, separation and purification of bioactive molecules.

- Citrus peel contains many molecules which can be used as natural additives in various industries such as pectin obtained from the peel by acid extraction and the whole dietary fibre obtained by mechanical processing (Djilas et al. 2009).

- Citrus peel represents a source of natural phenolic compounds, especially the characteristic flavanone glycosides, mainly naringin, hesperidin, narirutin, and neohesperidin (Bocco et al. 1998). Total phenol contents of orange peel ranged from 1.13 to 7.30 g/100g d.b (Cheynier 2006; Kammoun et al. 2011), respectively. The citrus flavonoids have been found to have health-related properties, which include antioxidant, anticancer, antiviral and anti- inflammatory activities. However, these by-products are sensitive to biochemical and microbial degradations because of their high amount of moisture (70-80%). Moreover, the phenolic compounds of citrus by-products could be submitted to enzymatic oxidation at different steps of processing.

Component parts include:
- Moisture
- Soluble Sugars
- Proteins
- Fat
- Flavanoids
- Essential Oils

*Source: Proximate chemical composition of orange peel and variation of phenols and antioxidant activity during convective air drying.
Journal of new sciences: Volume JS-INAT(9). Published september, 01, 2015 www.jnsciences.org*


![]({{site.baseurl}}/term-2/material/smoothiebike.jpg)

I sourced my material from a smoothie shop here in Barcelona called Smoothie Bike. They get through about 15kg of oranges a week. They produce organic juices and deliver by bike. They have a subscription service, and collect the glass bottles to re-use them. They were very supportive of the project as it supported a lot of their values. The contact i made Kevin also i think saw it as a business opportunity, with me being based in a university. Which is know bad thing, with getting people on board!

I was also collecting orange peel from people at IAAC as they ate oranges during the project.

**Material Testing:**

So its time to get in the lab. Get to know the material and see what I can do with it. The first tests that i want to do will be seeing what happens when i do some basic tests.

{% figure caption: "*Orange peel and boiling the first pieces*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/peel.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/boil.jpg"/>
  </div>
</div>
{% endfigure %}

{% figure caption: "*compressing boiled sample and drying strips of peel*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/boiledmash.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/dry1.jpg"/>
  </div>
</div>
{% endfigure %}

I blended the pieces after i'd cooked them. I was struggling to get the material to stick together. However it was only later i realised that i hadn't been leaving the peel long enough to dry properly.


{% figure caption: "*drying the strips and timings*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/drytimes.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/driedstrips.jpg"/>
  </div>
</div>
{% endfigure %}

I wanted to see how long it would take to dry the strips of peel, so chopped them up thinly and put them in a 50 degree C oven.


{% figure caption: "*chopping and grinding dried samples*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/chopdry.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/powderdry1.jpg"/>
  </div>
</div>
{% endfigure %}

The dried samples I then chopped up and ground to make a powder. Which I wanted to use with various binders to see if it would stick together.


{% figure caption: "*drying day 2*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/drystrips2.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/burntstrip.jpg"/>
  </div>
</div>
{% endfigure %}

{% figure caption: "*working with cornstarch*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/cornstarch.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/cornstarchsamples.jpg"/>
  </div>
</div>
{% endfigure %}

I mixed cornstarch with the orange powder and undried pulp. All the materials cracked on drying, and were not very strong.

{% figure caption: "*working with cornstarch and glycerol*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/glycerol1.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/glycerol3.jpg"/>
  </div>
</div>
{% endfigure %}

Adding glycerol to the mix made them more flexible, but still not strong enough to be able to make anything from easily.

{% figure caption: "*Samples laid out in drying area*" %}
![]({{site.baseurl}}/term-2/material/Orange-pic/collection1.jpg)
{% endfigure %}

I tried to remove the oil from peel, but soaking them in ethanol for a week. I then ground up the peel and tried getting it to bind to itself. However it failed badly. I don't know whether the oil was still present, or it was the effects of the alcohol, or whether it was the peel itself causing issues. Which is when more chemical analysis would be helpful. I did get some oil though from an otherwise unsuccessful process.  

{% figure caption: "*Cornstarch oven test*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/csoventest.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/chickenskin.jpg"/>
  </div>
</div>
{% endfigure %}

One of the cornstarch materials i accidentally left in the oven. It went really hard, so i tried to recreate it with various amounts of binder. It didn't really work however. The first sample just cracked like normal. The other samples that had less binder and more pulp just shrunk, and ended up with a consistency roasted chicken skin.  

{% figure caption: "*pulp tests*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/pulpcooked.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/orangebowl.jpg"/>
  </div>
</div>
{% endfigure %}

There was one sample of pure pulp that was dried for a long time in the oven. There were fibres that could be seen, that bound the sample together. It cracked a bit, and warped, but it was very hard. I tried to put in a mould of a metal bowl and left it in a 50C oven. Unfortunately the oven wasn't turned off as it should have been over night. The orange as result burnt. It also dried unevenly. The carbon remains were very tough however.

![]({{site.baseurl}}/term-2/material/Orange-pic/bowlcooked.jpg)
Over cooked orange bowl.

{% figure caption: "*grinding powder*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/grinder.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/drypowder.jpg"/>
  </div>
</div>
{% endfigure %}

We found that using a meat grinder enabled us to get a really fine powder from the dried samples. It smelled great!


{% figure caption: "*Hot oven tests*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests7.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests8.jpg"/>
  </div>
</div>
{% endfigure %}

I tried putting the pulp in the oven at 150 degrees C, so see what would happen if it actrually cooked, rather than dried. The material didn't crack as easily, as it developed a skin. However it lost its moisture too fast if the sample was thick and it became very uneven.


{% figure caption: "*Hot oven tests*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests10.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests12.jpg"/>
  </div>
</div>
{% endfigure %}

The samples cooked in the oven also lost the attractive orange colour. It looked a lot like cooked potato skin.

{% figure caption: "*Hot oven tests on moulds*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests13.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests14.jpg"/>
  </div>
</div>
{% endfigure %}

I wanted to see what would happen if i put the samples over a mould, so i used the metal petri dishes. I also tried mixing one sample of pulp with spent grain that another group was using. I wondered if the grain would add strength to the samples.

{% figure caption: "*Hot oven tests on moulds results*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests15.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests16.jpg"/>
  </div>
</div>
{% endfigure %}

The sample that was just pure pulp dried out well, but was very thin and ended up warping quite badly. It looked a bit like a dry avacado skin. The sample with the spent grain (on the right) worked well. It was dry and hard, and didn't crack. I wanted to see if i could keep the orange sample pure however so i didn't continue this direction. I think it works though, using the orange as a binder and the spent grain for strength.

{% figure caption: "*Sieve Tests*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests19.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests21.jpg"/>
  </div>
</div>
{% endfigure %}

I tried using two sieves as a moulds, as the moisture would be able to leave from both sides more evenly than using solid moulds. The material dried in the holes in the mesh, causing it to stick, which was a bit of an issue. I was also having real problems recreating a working pulp mixture. Once blended the mixture wasn't binding to itself at all. It meant that it would collapse the moment it came out of the mould.

{% figure caption: "*Sieve tests in hot oven*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests22.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/materialtests23.jpg"/>
  </div>
</div>
{% endfigure %}

I put this mixture between two sieves again. I cooked it in the hot oven at 150 degrees C for an hour, then left it to dry in the cooler oven of 50 degrees C. It worked pretty well, probably the most successful sample. However this was from the first mixture which bound to itself.

I realised at this point that the source of the orange peels made a big difference. The ones that had been used to make smoothies still had some fibres from the orange segments themselves. If the orange had been peeled and eaten, then the was only skin and pith. This meant there was proportionally more oil, and less fibres to give them material strength and allow it to bind to itself.


![]({{site.baseurl}}/term-2/material/Orange-pic/Finalboardorange.jpg)

All the samples laid out for the final presentation.

<br>

| Code | Ingredients                        | Activities                                                                                              | Result                                                                                                                                                                                               |
|------|------------------------------------|---------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| P1   | Orange pulp 7g                     | Boiled orange peel,  Blended until smooth  Put in oven 150C for 40min Thin sample                       | Dried out, went brown, hard but also very warped.                                                                                                                                                    |
| P2   | Orange pulp 43g                    | Boiled orange peel,  Blended until smooth  Put in oven 150C for 2hr                                     | Dried out, went brown, hard, but brittle. Layers started separating. Lost a lot of volume in moisture loss.                                                                                          |
| P3   | Orange pulp 7g                     | Boiled orange peel, Blended until smooth  Put in oven 150C for 2hr                                      | Dried out, went brown, hard. Looks like baked potato skin.                                                                                                                                           |
| P4   | Orange pulp                        | Boiled orange peel,  Blended until smooth Put over a metal ring.  Put in oven 150C for 2hr              | Dried very inconsistently, outside much faster than the inside. Warped and drooped a lot.                                                                                                            |
| P5   | Orange pulp 7g Glycerol 1g         | Boiled orange peel,  Blended until smooth Put in oven 150C for 2hr                                      | Dried out, went brown, , remains soft and flexible due to glycerol.                                                                                                                                  |
| P6   | Orange pulp 20g                    | Boiled orange peel,  Blended until smooth Put in oven 50C for 4hr                                       | Dried out, went brown, hard, but brittle. Layers started separating. Lost a lot of volume in moisture loss.                                                                                          |
| P7   | Orange pulp 7g Cornflower 0.5g     | Boiled orange peel,  Blended until smooth Put in oven 150C for 2hr                                      | Dried out, went brown, hard, sample size too small to be useful.                                                                                                                                     |
| P8   | Orange pulp                        | Boiled orange peel,  Blended until smooth Spread over mould Put in oven 150C for 2hr                    | Starting drying well, without cracking. Now quite brown. However warped badly as it dried off the mould. The material is rather thin as well. Almost like caramel.                                   |
| P9   | Orange pulp Mixed with spent grain | Boiled orange peel,  Blended until smooth,  mixed with grain Spread over mould Put in oven 150C for 2hr | Bound together well without cracking too much. Light and rigid. Sample size a bit small.                                                                                                             |
| P10  | Orange pulp 35g Glycerol 3g        | Boiled orange peel,  Blended until smooth,  Put in a petri dish.  Put in oven 150C for 3hr              | Over cooked, went very black, lost volume too fast and sunk. Slightly flexible due to the glycerol.                                                                                                  |
| P11  | Orange pulp 35g Glycerol 3g        | Boiled orange peel,  Blended until smooth, Spread over mould Put in oven 150C for 2hr                   | Dark brown in colour, Slightly flexible due to the glycerol. Rips easily due to thin wall section.                                                                                                   |
| P12  | Orange pulp 35g Glycerol 3g        | Boiled orange peel,  Blended until smooth, Spread over mesh Put in oven 50C for 4hr                     | Didn’t bind together well. Too thin, and comes apart easily. Slightly flexible due to the glycerol.                                                                                                  |
| PB1  | Orange Pulp                        | Pulp boiled for 2hr Pulp blended  Put over mesh Air dried 2 days                                        | Cracked really easily and didn’t come off mould well.                                                                                                                                                |
| PB2  | Orange Pulp                        | Pulp boiled for 2hr Pulp blended Put over mesh 50C over 4hrs                                            | Cracked really easily and didn’t come off mould well.                                                                                                                                                |
| PB3  | Orange Pulp                        | Pulp boiled for 2hr Pulp blended  Put over mesh  Oven 50C 4hr                                           | Hard, rigid and held together well. Light brown in colour. Slightly brittle. Can see fibres in the material and takes on mesh of the sieve. Edges poor and ragged.                                   |
| PB4  | Orange Pulp                        | Pulp boiled for 2hr Pulp blended  Put over mesh Put in 150C oven for 1 hr then 50C 4hr Air dried 2 days | Hard, rigid and held together well. Dark brown in colour. Slightly brittle. Smoother than the previous sample. Can see fibres in the material and takes on mesh of the sieve. Edges poor and ragged. |

## Final Recipe

After all my experiments i realised that one of the simplest recipes was the one that worked best.

**Ingredients:**
- Orange peel sourced from juicing process. It needs some fibres from the oranges themselves still present with the peel.

**Method:**
- Boil peel for 2 hours.
- Blend pulp in food processor, blend until smooth paste. However don't over do it so some fibres remain.
- Spread evenly over mould with moisture able to escape evenly. E.g mesh
- Dry in oven for 1hr at 150 degrees C.
- Then remove and transfer to cooler oven about 50 degrees C  for around 4-6 hours.
- If you have the time don't transfer to cooler oven and allow to dry in the air for at least 24hours.

{% figure caption: "*Final Sample*" %}
<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/finalsample1.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/material/Orange-pic/finalsample2.jpg"/>
  </div>
</div>
{% endfigure %}

{% figure caption: "*Final sample with money (for scale)*" %}
![]({{site.baseurl}}/term-2/material/Orange-pic/finalsample3.jpg)
{% endfigure %}

## Reflections

**Sourcing:** I found it harder than I thought to get some of my material. Some shops i went into didn't want to give me their waste, even though it was just being put in the bin. There are strange rules that big companies have with what they do with their rubbish, particularly food waste. This can be frustrating. Finding Smoothie bike was good, as they fit quite well with the ethos of the project. However if i wanted to scale up the project i would have to find a bigger source. Like a bigger smoothie or juice company. Bike smoothie have an unreliable supply which wasn't a problem for this project but could be if i needed a larger supply of orange peel in the future.

I made an error also mixing my source material. I should have realised that having peels that were removed by hand would be different to ones which were juiced. However, it's all part of the process, so it's good to know now.

 **Process**: Firstly i really enjoyed the process, it was very different to anything i've done before. It was engaging with a material in a new way for me. I got rather excited about trying things, and maybe wasn't as rigorous as i should have been. Moving forward i need to record all tests and information much more carefully.

 I should really have started by doing as many tests as possible with just the peel on its own. Instead i used other binders as well, just because i wanted to see what would happen. The process would've been improved if i had stuck to just the peel, then gradually added other ingredients in bit by bit if required.

 The spent grain was promising i feel, and would like to explore future tests with that one as a potential future material.

 The sample sizes i created were often too small, so i couldn't really get proper information on how they behaved.

 We were only really looking at the physical properties, as that's what we could do in the workshop. It would be good to be able to really understand the chemical composition, and accurately test things. However i realise we are not chemists, and we only had a limited time frame.

 Due to me not being able to recreate my samples properly, i'm overall disappointed that i didn't actually create a product. I just about covered a mesh in the material. However it doesn't really change very much how the material behaves beyond a flat sheet.

 **Links to my project**: This project was always very much within my area of interest, which was great. Creating circular systems using biodegradable and natural waste products. Exploring materials in a digital space and not just discarding them without much thought, which can happen now so often.

 I'm currently thinking on how projects like this can be introduced to more people. Who can then learn about circular systems, and using biomass waste to create things. Could this also be an interesting engagement tool? Getting them to consider the harmful and wasteful practices we currently engage in, and try to nudge them into at least considering alternatives. Running workshops where people use recipes to make products using these more eco-friendly alternatives.

 One problem for all these new materials is how to scale them up and how to get them into the world. If a new material has similar properties to an existing one, a limiting factor can be price. A new material might not be able to compete with existing processes, or its not made at a scale that allows for it to be made for a low cost. People still want cheap options, that are easily accessed. Business models often don't allow more expensive alternatives even if they are more sustainable. What incentives can be provided that encourage people to demand less harmful or wasteful material use?

 **Next Steps**: I want to carry on the project. See that with more time i can make a more finished product. I would like to mould it properly if possible, and see if it can be finished or even water proofed. I think it could be sanded, and with the addition of something like pine resin polished to a high level. I want to carry on the project and see if packaging could be made from the peel.
