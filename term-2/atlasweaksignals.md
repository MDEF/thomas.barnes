---
layout: page
title: Atlas of Weak Signals
permalink: /atlas of weak signals/
---
![]({{site.baseurl}}/images/AoWS map.png)


This term we had the Atlas of Weak Signals seminars. Looking at themes which contain weak signals, and how they relate to our projects and us as designers for emergent futures. There was the definition track, where we went into details about what these emerging issues are. We then came up with speculative scenarios to how these themes could be tackled, or at least looked at differently. Then there was the development track. An attempt to identify these signals, and map / catalogue them in a way and see how they connect.

**What is a weak signal?**

‘Something that is almost impossible to detect from background noise, which could be important for the future, could cascade into changes, or could be unimportant or irrelevant.’

These are things which in their nature are very difficult to identify. They are indicators, potential clues. As we can’t predict the future, they could however end up leading no where. For example identifying overuse in pesticide use by lack of birdsong in woods. The observation sometimes is not directly linked to the cause, but it’s a way of identifying a potential pattern of behaviour.


<p align="center">
<b>Anthropocene </b><br>

<br>

Longtermism <br>
Fighting anthropocene conflicts (movement of species)<br>
Carbon neutral lifestyles<br>
Interspecies Solidarity<br>
Climate Consciousness<br>
<br>

<b>Surveillance Capitalism:</b><br>
<br>
Attention protection<br>
Dismantling filter bubbles<br>
A circular data economy<br>
The truth wars (Fake news)<br>
Redesigning social media<br>
Manipulation<br>

<br>

<b>Future of work</b><br>
<br>

Future Jobs<br>
Human-machine creative collaborations<br>
Fighting AI Bias<br>
Tech for equality<br>
Making UBI work<br>
<br>

<b>The end of the nation state</b><br>
<br>
Welfare State 2.0<br>
Making world governance<br>
Rural futures<br>
Pick your own passport<br>
Refugee tech<br>
<br>

<b>Identity</b><br>
<br>
Non heteropatriarchal innovation<br>
Imagining Futures that are non western centric<br>
Reconfigure your body<br>
Gender Fluidity<br>
Disrupt ageism<br>

</p>


I started my project with a very simple driving force, reducing waste. Which i saw as a result of a consumerist, market driven economy. Seeing the effect that it has on the world, the **Anthropocene** is the overarching theme that connects all the other ones. How do we deal with an ecological breakdown that is being seen already to have dramatic effects. Climate change is the catalyst which will effect how the world works in the coming decades whether we like it or not. However there is still a large gap between **climate consciousness** and behaviour. Even with people who are well aware of the effects of their lifestyles. It is still problematic to change.

Then there are so many distractions, the **attention protection** issue, not just on a personal level. Which is important. Our attention however is being distracted all the time from important issues by other ones. Greta Thunberg is in the news all the time now due to her passionate action on climate inaction. When addressing MEPS in the European Parliament ‘her speech harangued them for spending too long on Brexit and not enough time on the climate’. The **truth wars** come into play as big global players try to distance themselves from climate reforms. Scientific narratives are increasingly being attacked, and people choose to believe things that suit them, and enable them to live in way that doesn’t disrupt their current lifestyles.

We focused on water in one of our week’s scenarios. Thinking how **climate neutral lifestyles** will be required to mitigate some of the effects of water over consumption and scarcity. It was amazing to see how much water goes into everyday items. It is one of the most precious resources we have, and yet there are many situations when it is wasted terribly.

![]({{site.baseurl}}/images/watercosts.jpg)

My project is also linked to attaching materials to more natural cycles. How can we learn and cooperate with natural processes? As opposed to just extracting and dominating? Its this **interspecies solidarity** which is also very important to overcoming issues of how we operate. In terms of thinking about natural materials this concerns all sorts of life forms. From bacteria to plants, insects to large animals. For our final presentation we explored the theme of bees quite a lot. It crossed ideas such as collective consciousness, insect population decline, communication, and ecological dependency. We also touched in on **future jobs** that might occur as a result of changes in attitudes towards the natural world. We created a bio-influencer. Someone who is dedicated to trying to change how people use materials, by accessing more natural flows and processes.

**Identity** is one topic in which we didn't get to go into in too much detail. Jose Luis made a point of not wanting to be lecturing identity as a white western middle class male. Which is a fair enough point, however being from a similar background makes me think about how we should use the positions we are in. Take advantage of the 'privilege' to support others in not such advantageous positions. To not pretend we know what its like for someone of a different background, but to not ignore or exploit certain situations either. We are at a time when there is a lot of discussions over people identity. I feel that there is a lot of more tolerance, but there's also more visible intolerance. We have platforms which enable people share their views, in positive and unfortunately detrimental ways.

Who we are, and what we stand for will come under increasing scrutiny. In political arenas views are becoming more polarised. There is a tension between **global** and **local** interests. We are connected more than ever before, and i think that it gives us a responsibility to support people that are near us and far away.

<br>

<iframe width="640" height="360"
src="https://embed.kumu.io/d63698d2aac84803bdd4dba1cc6bfbaf"
 width="940" height="600" frameborder="0"></iframe>


### Reflections

Process

Definitions: I found the whole idea of scenario building really fascinating. To force yourself to see things differently, to imagine different scenarios is challenging. It’s very easy to dismiss ideas, and say ahh they wouldn’t work. Inevitably someone will pick holes in an ideas, however that wasn’t the point of the exercises.

My key areas i feel are **climate consciousness**, **carbon neutral lifestyles**, **truth wars**, **attention protection** and **interspecies solidarity**. 

Development: We used an exercise of data scraping to gather key words from websites that reflected ideas we had been exploring in the definition track. This was done using python scripts and a data scraping tool called ‘Beautiful soup’. However I wasn’t totally convinced by the exercise. I think this was because we found websites which were probably too obvious, they were showing strong signals about the topics we were looking at, not weak ones. This meant that the keywords were not hugely helpful. We had connecting words like ‘sustainable’ and ‘people’. Maybe we need more data sources, and  work harder identify unusual patterns.

The one thing that the mapping exercise goes to show is how these themes are all connected in one way or another. The complexity involved with all the issues are scary, and that is the point. How we focus in one some of the issues is key here. We can’t individually deal everything, and I think I have suffered in my project because I have tried tackle things to broadly. I’ve blocked myself by not taking one aspect, running with it and building other issues on top as I go.
