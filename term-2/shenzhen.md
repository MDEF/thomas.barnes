---
layout: page
title: Shenzhen Research Trip
permalink: /shenzhen/
---

**Introduction**

This page documents the trip that MDEF students and staff took to Shenzhen in China between the 8th and 14th January 2019. What was this trip all about? It was a chance to visit one of the most dynamic innovation centres in the world. A city where much of the planet’s electronics are designed and made. It is a Chinese experiment that has transformed a series of fishing villages into a megalopolis in only 40 years. We visited design studios, maker spaces, factories and vast electronics markets. It was an opportunity to see where a lot the products we are familiar with originate, and how the approach to design and making contrasts with what we know back at home.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/lift_city.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/night_road.jpg"/>
  </div>
</div>

Places we visited over the week:

**SZOIL**

The first place we visited was Shenzhen Open Innovation Lab (SZOIL). We saw various design companies based in the complex. SZOIL is also a Fablab dedicated to connecting makers to the making eco-system in Shenzhen. We had an introduction from the space’s founder David Li. Who went through some of the key features of Shenzhens approach. It is a complex system, but one focussing on collaboration. No one does every thing, and knowledge and skill sharing is key to success. Products were traditionally aimed at specific niches, where they would corner a particular area of the market rather than going for the whole. Another key thing is there isn’t a strong focus on uniqueness, being copied is a sign of success, that you are doing something right. Therefore people aren’t afraid of it. There is also a very fast approach to product development, where people will just try and test things in the real world. If they work they will carry on, if not they will stop and move onto something else. This means that they can get something to market in around 3 months, and then another 3 months after that similar products may appear from competitors.


**Huaqiang Bei**

Huaqiang Bei is a huge electronics market. It is the human facing front of many of the factories. Where you can find almost any hardware to make almost anything. As long as you can navigate it’s systems and have some Chinese speaking support. The sellers on the stalls negotiate huge deals for the factories they represent, with many people ordering items in the thousands. There is a huge amount of knowledge in these markets, and a large repair market, particularly for smart phones. Most of the worlds smart phones get produced in Shenzhen so they know how to keep them going and have spare parts readily available.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/hb1.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/inside_market.jpg"/>
  </div>
</div>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/hb_floors.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/city_car.jpg"/>
  </div>
</div>

{% figure caption: "*Strict signs about behaviour in the markets*" %}
![]({{site.baseurl}}/images/shenzhen/no_frolic.jpg)
{% endfigure %}

<br>

**Seeed Studio, Shenzhen X.Factory**

Seeed Studio is an electronics company, which focusses on supporting makers, and supplying components that enable their work. They also set up the first maker space in Shenzhen called X.factory, which we visited as well. The company's founder and CEO Eric Pan discussed the interest there is for global conversation. For industries in China to work with makers who can help them remain competitive, and tackle other issues like sustainability and environmental impact. A lot of the manufacturing knowledge remains in China, and doesn’t get transferred back to designers in other countries.

**Urban Village**

 We visited the very Chinese phenomenon of Urban villages with Jason Hilgefort. These developments are what are left over of the villages that were swallowed up by Shenzhens huge growth. They are unique because the government gave control of these villages to the residents and their families. They became places that the large numbers of migrants to the city could find residence. In a city where space is such a premium they have also made a lot these former villagers very wealthy. As they have sold their land to developers. A lot of them are characterised by *woshoulou* or *hand-shaking building*. Blocks built so close together that people can reach across narrow alley-ways and touch. They remain options for low-income members of Shenzhen society. They are also vibrant spaces, with strong comunities and reminders of a recent past in a constantly changing and still very young city.

 <div class='twin-image'>
   <div class ='column'>
     <img src="{{site.baseurl}}/images/shenzhen/urban2.JPG"/>
   </div>
   <div class ='column'>
     <img src="{{site.baseurl}}/images/shenzhen/umbrellas.JPG"/>
   </div>
 </div>

 <div class='twin-image'>
   <div class ='column'>
     <img src="{{site.baseurl}}/images/shenzhen/urban_alley.jpg"/>
   </div>
   <div class ='column'>
     <img src="{{site.baseurl}}/images/shenzhen/man_dog.jpg"/>
   </div>
 </div>

**Moulding factory, PCB plant, Vanke construction site, Chaihuo X.factory - Dongguan**

We were taken by Seeed to look at some of their partner's factories in the nearby town of Dongguan. This included the compnay *Shenzhen Strongd Model Technology Ltd*, where they specialise in prototyping and (reasonably) low volume injection moulding. They machine their own injection moulding tools.

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/injection.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/boxes.JPG"/>
  </div>
</div>

We visited a PCB production plant, run by *Shenzhen Xunjiexing Technology Corp. Ltd*.

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/pcb_making.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/copperpcb.JPG"/>
  </div>
</div>

*Vanke* is one of China's biggest residential real estate developers. We were shown one of their construction sites, where they were making a 55 story skyscraper.


<div class ='center-image'>
   <img src="{{site.baseurl}}/images/shenzhen/vanke_site.JPG"/>
 </div>

![]({{site.baseurl}}/images/shenzhen/concrete_pour.JPG)

Chaihuo X.factory is Seeed Studio's maker space based in Dongguan. Being based in manufacturing area of Dongguan it's aim is to connect maker's with manufacturers.



<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/xfactorydon.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/cha_makers.JPG"/>
  </div>
</div>

![]({{site.baseurl}}/images/shenzhen/makermap.JPG)


**Other places visited:**

Museum of contemporary art

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/museum.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/museum2.JPG"/>
  </div>
</div>

<div class ='center-image'>
   <img src="{{site.baseurl}}/images/shenzhen/silver_sculpture.JPG"/>
 </div>


**Circular Economy**

As part of the week we were asked to examine an aspect of Shenzhen in relation to our areas of interest. Our group looked at circular economies, and how it manifests in Shenzhen. Like in most of the world, the systems are quite linear. With the majority of the worlds electronics leaving the city and not returning. However there is vast amount of knowledge, skills and equipment. All which mean that there is actually quite a large repair culture. With electronics, and particularly smart phones being collected from all over China and brought to Shenzhen. There is also a market for rejected parts that come out of the factories, where people salvage what they can and make cheaper items from these cast off components.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/fix_phone.JPG"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/images/shenzhen/repair_comp.jpg"/>
  </div>
</div>


**Reflections**

The trip was definitely as fascinating one for me. In a way because i had a lot of preconceptions about what Shenzhen would be like, and it was what i thought it would be in a lot of ways, but very different as well.

In it feels quite similar to many modern cities. However, it's scale and age makes is very different from many other cities i've been to. The people were on the whole really friendly and helpful. They made it much easier when we were in difficult situations, even with some impressive language problems.

I was hoping to see some really crazy, niche products. Which i struggled to find. There were lots of gadgets, and products that i was very aware of already like drones and electric scooters. However i wasn't there long enough to really hunt out the more obscure items that i'm sure are to found there. Also its important to consider that companies will mostly be producing the most popular items, ones which will sell in large quantities. This means that despite its history of creating products for specific segments of society, the most widely used products will be the most obvious and widely produced.

Shenzhen has up skilled itself to a point that it is no longer the cheapest place to produce products. Manufacturers are looking at a real possibility of having to close their factories and moving to cheaper places if they don't diversify their production. They are struggling for ideas, so are welcoming external support. Could this be an opportunity for creating a products that are most environmentally sustainable. That could they actively shift towards greener approaches to making items. However this will only happen if the incentives are there and the opportunity for success is a real possibility.

There is a wealth of knowledge and talent in Shenzhen. Will it stay there or could it be shared more effectively? Could it continue to adapt and absorb other influences from global partners? How will a city which has changed so much in the world continue to affect it in the next few decades?
