---
layout: page
title: Design Project
permalink: /designproject/
---

![]({{site.baseurl}}/term-2/designproject/mat-lit.jpg)

<a name="Back to top"></a>

## Introduction

This page is dedicated to documenting the main MDEF design project. A place for research, ideas, tests and reflections. How can materials, sustainability, and digital fabrication change the way we use and produce in society today?

## Contents
<a name="Back to top"></a>

<a href="#Background">Background</a>

<a href="#Shenzhen">Shenzhen</a>

<a href="#Material Literacy">Material Literacy</a>

<a href="#Term 2 Mid-Term Review">Term 2 Mid-Term Review</a>

<a href="#Material Experimentation">Material Experimentation</a>

<a href="#Decisions">Decisions</a>

<a href="#Term 2 Final Presentation"> Term 2 Final Presentation</a>

<a href="#Reflections">Reflections</a>

<a href="#Research">Research</a>


<a name="Background"></a>
## Background

This project began with a desire to reduce waste, for me in a professional setting as a maker, and at home as well. I grow increasingly alarmed at the amount of things that are thrown away. Activities that attempt to address some of these issues such as recycling have issues and do nothing to tackle root causes. I come from a digital fabrication background, and being based in the Fab Lab in Barcelona I decided to focus my area of interest in that context. What are the issues surrounding material sustainability in Fab Labs. How are materials used and processed? What is thrown away and what can be reused?

Sustainability in Fab Labs faces similar challenges to other areas in society. The systems and flows are based on linear models. Technical processes and materials are not suitable to circularity. There are toxic materials being used and machines with high energy consumption. They are also environments which are hard to measure and track, with multiple activities and agents working simultaneously. “Even actors with a clear ecological mandate struggle to engage with emerging sustainability issues in a rapidly changing socio-technical environment.”¹ Fab labs are also very much driven by the technology and processes they posses. The substrative technologies like laser cutting and CNC machining always have a point that means they cannot be optimised any longer and have waste of some sort. The most difficult material currently for the labs to deal with is wood, with no one wanting to take offcuts which vary in type, size and standardisation of supply.

This said Fab labs are also environments which have massive potential to tackle such issues. They are places of learning, experimentation and innovation. This means that there will be waste produced, as prototyping is inherently material heavy, as products are iterated thought the design process. There are currently projects like precious plastics, that engage with the issue of plastic waste. It uses the technical ability of plastics to be melted down and remade as a technical process and a model to campaign about these issues. Fab labs also have the technology to adapt and up-cycle old products. We ourselves on the MDEF course found materials on the street and made furniture out of it.  

As part of the work undertaken to develop our areas of interest I created a scenario which aims to tackle some of these issues.

**Scenario**

***Fabs labs have easily accessible knowledge about material choices which include innovative, accessible and non-toxic options. They have engaging learning methods to teach Fablab users about the options, contexts, benefits and limits of various materials.
Incentives are given for re-using materials and a clear system is in place to trace materials as they flow through the lab. No materials end up in landfill, as all materials that can’t be re-used in-house have other uses for external Fablab associates and organisations.
Toxic materials aren’t banned outright, but they are discouraged, strictly controlled and kept separate from non-toxic options.
Space, time and value is given to processing materials, as an important part in the design and making process.***

![]({{site.baseurl}}/term-2/designproject/fablabtimeline.jpg)

I highlighted three areas of intervention

- Stage 1: **Reading the system**
- Stage 2: **Into the system - Material Literacy, Material Access and Design Decisions**
- Stage 3: **Out of the system: Re-use waste, Re-cycle Materials. Connect remaining materials to local eco-systems:**

Stage 1 involves a quantitive approach to finding exactly what is happening in the lab. What materials are used, how much, how efficiently etc. Stage 2 is more about the decisions made before anything is made, what effects those decisions and how can they be influenced to create more sustainable outcomes. Stage 3 is about what happens after products are made, how are they, and any waste dealt with in

The activities in Stage 1 are important however, ultimately weren’t ones which excite me very much. There is also currently work being done in the Barcelona Fab Lab to do the same work, by Milena Juarez Calvo, and environmental engineer. I didn’t feel therefore I wanted to duplicate her work, and hopefully could help with it in some way further down the line, or use the data gathered to support some of mine.

{% figure caption:"Material Flow begins, but problems of people misusing bins currently exists"%}
![]({{site.baseurl}}/term-2/designproject/binmatflow.jpg)
{% endfigure %}

<br>
I find there are similar limitations to Stage 3, as Fab Labs are already trying to engage with these activities. I also think that a greater affect can be harnessed at the start of the design process. Rather than trying to deal with issues after they are made. For this reason my intervention area focusses on Stage 2 - Material Literacy, Material Access and Design Decisions.

<a href="#Back to top">Back to top</a>


<a name="Shenzhen"></a>
## Shenzhen

The Shenzhen research trip in January provided insight to the worlds electronics production and development, where technical advancements are huge. It was quite hard however to see the world’s obsession with materialism on such a large scale. Shenzhen is the physical source, but not the cause of the problems created. Which we are all complicit in sustaining. China suffers by being one of the worlds industrial powerhouses, particularly with its air and water. There is interest and need however to tackle these issues, see (SEEED studio). It will be fascinating to see if China manages to progress faster than a lot of other countries, which struggle to engage and even accept there are any problems.
The issues surrounding electronic waste, material use, and environmental degradation are complex. However the Shenzhen approach to collaborative work, fast iteration and fearless testing can be seen as being applied in maker spaces and Fab Labs already. There is just a disconnect at the moment with ideology and reality. My hope is that these spaces will continue to push research and application. As always then the challenge lies with increasing engagement, and scaling up of ideas.

<a href="#Back to top">Back to top</a>

<a name="Material Literacy"></a>
## Material Literacy, Material Access and Design Decisions.

<br>

Why do we choose certain materials? Why are so many popular materials bad for us and the environment? There are so many new materials being developed, but why are the same ones being used repeatedly? How much do we really know about materials we come into contact on a daily basis?

There are three factors which are normally the key ones to why we choose to design with or use certain materials.

**Function - Cost - Access**

What are the properties of a material, are they suitable for my application?
How much does is cost, can I afford to use it?
Can I get hold of it?

This approach works at a very basic level, and I see it in Fab labs all the time. The classic case study material  for me being MDF. A ‘recycled’ wood, but using fibres bonded together with formaldehyde glue. Its cheap, easy to cut, glue and finish and can be accessed easily. However it is also toxic, and really hard to recycle at it’s end of life.

Possible avenues of approach:

Challenge current normality - Look at the aspects of **behaviour, culture, expectation**. If we look at the transition points of these then how they could be influenced.
In Fab labs the variables are people, materials, processes. How they are changed also affects the system.

**Ideas**

**Material libraries**: One things that has always struck me in Fab labs is that they are so focussed on their technologies not on materials. The materials end up being a limiting factor, being chosen because they work with a particular process. There are lots of exciting new materials being developed, we don’t always see them though. There is no fixed space in a Fab Lab for materials, or material development. A start could be material library of existing materials. Moving to new and interesting ones, ones that can be accessed locally, or have a sustainable element.

An amazing example of this is Materfad. Which I’ve now visited twice. There are lots of innovative materials there, all commercially available. Can this be replicated in some way in fablabs? Providing tactile examples.

![]({{site.baseurl}}/images/materfad/materfad_entrance.jpg)

![]({{site.baseurl}}/images/materfad/samples.jpg)

**Green Materials**: One option is to try and phase out materials that are harmful to the environment, and that are hard to process or repurpose. One example of a material trying to do that is [Chips board](https://www.chipsboard.com). A board that is similar to MDF or regular chipboard. It is however made of discarded potato peels. Are there other similar more eco-friendly materials that we use, although the challenge is to be inspiring and not restrictive about it.

{% figure caption:"Chipboard in use"%}
![]({{site.baseurl}}/term-2/designproject/p-1-chipsboard.jpg)
{% endfigure %}

**Design for Disassembly**: Another challenge is when we combine materials is how hard it is or not for them to be recovered once the products life is over. Are there techniques that could be explored to try and improve this process when prototyping?? Again challenging if this process ends up driving a design.

**Tool Kit**: Through trying to source materials, creating a library, making items that can be disassembled, working on Fab academy assignments and not buying or throwing anything away. Could a tool kit be developed? A set of approaches that come out of this research? What are the challenges and successes, that other people could use who are wanting to design or prototype with similar ideas in mind?

**Material Driven Design**

As part of MDEF we have done a Material Driven Design module. This is very much aligned with my area of interest. In this we all had to take the challenge of creating our own material, out of biomass. That is seen as waste product currently, and that is fully biodegradable. I chose to make material out of orange peels, and documentation of that process can be seen [here](https://mdef.gitlab.io/thomas.barnes/material%20driven%20design/).

![]({{site.baseurl}}/term-2/designproject/orangesample.jpg)

The aim of this course is to make us consider materials as more central to the design process. To have a connection to them in a way that craftspeople traditionally had. Digital design processes can lead to a disconnect, where people don’t know fully what they are using and material decisions are an afterthought.

I found the process really interesting, struggling through what the possibilities are with a material. Thinking about bigger considerations of origin, use and disposal. The project is only a short one, so I definitely want to carry on for the rest of the course. Seeing how far I can push the material capabilities in a hands on way.

**Material Literacy**

Material information can often take the form of quite dry textbooks and online databases. The libraries are have samples but are often out of context.

- How can we combine physical materials with learning about their properties?
- How can we engage with people so they can understand more about the materials they use?

My focus started in Fab Labs, and will remain connected to them. However I’m widening my field of interest a bit. I’ve found the nature of fablabs makes this area a bit restrictive. Whether there is a different approach I can either test or bring back later on.
For now I want to look into issues of material literacy in different contexts and to different audiences.

One thing I was really interested in when i visited Materfad was how they engage people with the issues surrounding material use. They have the physical materials, applications and most importantly engaging explanations. Robert Thompson, an Industrial Engineer who works there gives incredibly inspiring speeches, and is able to traverse different levels in discussing different materials. This ability to inspire is what is important. Otherwise people will just keep using the same matrix for material selection.

**Material Ecology**

One of the problems today is how we see materials. We see them as static objects that exist in isolation to their surroundings. We are always told how plastics are inert and will never break down. This doesn't mean that they don't interact with their surroundings however. Same as other materials that move, deteriorate and change over time.

This material ecology is described by the anthropologist Tim Ingold. This angle surrounding material culture is a fascinating one. Which advocates for a more dynamic approach and 'it is to these flows that we need to attend if we are to understand making'. We have become disconnected from the very systems that sustain us. Our material choices reflect that, when we choose to use certain materials. There's a paradox that some of the most amazing materials, with great properties are so harmful to us or the environment.

If we want to look at the state of the art we only need to look at Neri Oxman and her take on material ecology. Working at intersection of computational design, additive manufacturing, material science and synthetic biology. Where nature inspires to create projects that are at the boundaries of what are currently possible. Redefining how we create the world around us.

<p align="center">
Static - Movement <br>
Uniform - Growth <br>
Quantifiable - Adaptable <br>
Machined - Symbiotic <br>
</p>

**Material Responsibility and Hypocrisy**

Another aspect i've been looking at recently is the issue of material responsibility and hypocrisy. It is one thing to identify the issues of using certain materials or products that have negative environmental or social implications. However most of us still use them anyway. These are some of drivers here which seem to dictate people's choices.

**Ethics** - We should reduce our environmental impact. Its the right thing to do for our planet, for people living now and for future generations. To ignore it is morally wrong.

**Economics** - Even if the above is true, and we want to live consciously we need to be in a position to do so. However, many ecologically friendly products are more expensive than more harmful ones. This means that often we won't make a choice that is better because we can't or don't want to spend the money on it. There are the incentives in place to make other choices.

**Expectation** - There is an attitude that is quite common, which is as long we can afford to do something then it is fine.

**Behaviour** - Some options are just less convenient, so we will justify actions because they are easier in that current moment.  

It is a really hard thing to provide responsibility to things we use and consume. Particularly when we can't see the results directly. Are there better feedback mechanisms that can be created make people more aware of there actions. That can provide more mechanisms for choice, that incentivise positive choices, and nudge people away from easy but harmful ones.

<a href="#Back to top">Back to top</a>

<a name="Term 2 Mid-Term Review"></a>
## Term 2 Mid - Term Review

We had a Mid Term Review on 5.03.19

Here is the presentation based on the work and concepts I've been looking at:

![]({{site.baseurl}}/term-2/designproject/mtmidreview1.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview2.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview3.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview4.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview5.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview6.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview7.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview8.jpeg)

![]({{site.baseurl}}/term-2/designproject/mtmidreview9.jpeg)

Feedback

- There is definitely a problem with waste and overconsumption, which people have known about for a long time. What am i actually proposing here though? The ideas of material literacy is an interesting one. Giving people the tools to make decisions on their material usage. However it needs to connected with actual workshops and frameworks that would excite and inspire people.

- I need to stop looking the problems and start suggesting ideas for new ways envisioning future scenarios. It is difficult to give feedback without ideas on the table.

- Get a context, and construct a narrative which can develop.

<a href="#Back to top">Back to top</a>

<a name="Material Experimentation"></a>
## Material Experimentation

![]({{site.baseurl}}/term-2/designproject/Waste-material-plan.png)

**Design actions:**

Working with waste materials from the lab:

Following on from the material driven design module I continued to try and experiment with materials. I made some basic presses to try make sawdust from the lab into boards. Mixing the sawdust with cornstarch as a binder. The difficult bit is getting the boards to dry evenly. They really need to be pressed very firmly, and then be able to have air circulation for the moisture to evaporate. I tried with fine mesh, but found that the material stuck very easily to the boards. This can be rectified by adding baking paper, however then the moisture doesn’t escape easily. Metal sheets in the press with holes in it might work better. I also continued experiments with orange peel, trying to get it to bond properly was really difficult. The oil causes the material to crack really easily as it dries. I also struggled to rectify past experiments.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/designproject/Test1.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/designproject/Test2.jpg"/>
  </div>
</div>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/designproject/Test3.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/designproject/Test4.jpg"/>
  </div>
</div>

![]({{site.baseurl}}/term-2/designproject/Test5.jpg)


I also tried using pine resin on the orange peel to see if it bonds easily. It does form a really nice clear coating, but might be structurally a bit weak, crack and scratch readily. Pine resin is also really difficult to work with, it is so sticky, and then dries really hard on everything.

![]({{site.baseurl}}/term-2/designproject/pineresin_orange.jpg)

Limonene and polystyrene. Limonene is one of the active chemicals in orange peel, and it melts polystyrene. It is used in the lab to melt HIPS from 3D printed builds. I did an experiment to melting some samples of expanded polystyrene. It can be recovered from the limonene solution using alcohol, and then dries to a hard polymer. This can then be used to make items again. Expanded polystyrene takes up a huge amount of space in landfill. One of the big issues with this as a process is the cost of the limonene extraction from orange peels.

Making cakes: One of the issues I’ve seen in maker spaces and manufacturing in general is the exposure to dangerous chemicals. There are procedures in place to limit the dangers of certain chemicals to those working with them, particularly in more developed countries. However these are not always adhered to strictly. I therefore made the cake out of waste materials in the lab, mainly wood and plastic. You wouldn’t put this cake in your mouth, however we are potentially putting dangerous chemicals in our bodies through contact via our skin and breathing in dust and small particles. I also saw a connection between this behaviour and our attitude towards food. Cheap, accessible food is often prioritised over other more ‘healthy’ options. Our food chain is full of chemicals to artificially grow, enhance and preserve food. Lot of which can have very negative effects long term. There is also growing evidence of the waste we produce getting into our food chain, with micro plastics getting into our bodies through food we eat and our built environments. This comes full circle when we think about using biomass waste for new materials. Ones which will work better with natural cycles and are less detrimental to human health.

<br>

<div class='twin-image'>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/designproject/cake1.jpg"/>
  </div>
  <div class ='column'>
    <img src="{{site.baseurl}}/term-2/designproject/cake2.jpg"/>
  </div>
</div>

![]({{site.baseurl}}/term-2/designproject/cake3.jpg)

<a name="Decisions"></a>
## Decisions

<a name="Term 2 Final Presentation"></a>
## Term 2 Final Presentation

![]({{site.baseurl}}/term-2/designproject/presentation_table.jpg)


I feel I have gone round in circles a bit this term, thinking about area of interest, materials and waste. I still don’t have a clear intervention, so I discussed the things I have been looking at with the hope that the experts that came could point me in a direction I can follow in more detail in the last term.

**Alternative Material List - Liz Corbin**

Fab labs all have very similar machines and processes, which enable them to make awesome things globally. They also have materials which are very similar, and include Acrylic, ABS, MDF, Plywood, PLA, Cardboard. These materials are often extracted in unsustainable ways. Her suggestions were to look in detail at alternatives materials that are more sustainable and that can be made locally. Then communicate these alternatives in an interesting and engaging way. Materiom can help with recipes, the issue being is I don’t now have much time. I could alternatively choose a more object based approach, and show how very commonly made objects that are made in fab labs could be made using alternative materials and methods. MIT is also looking at this, and Neil Gershenfeld wants to have a like for like alternatives for materials currently used. However Liz thinks this is unimaginative, and there is scope to use nature’s way of creating objects in a much more symbiotic way.

**Laura Cléries**

I need to do many more experiments if I’m going down a material route. The biggest problem is scaling up and tests to provide a substantial amount of the material. Not just have them in petri dishes, but usable amounts. If I’m going to look at a waste collection platform then I need to connect more with the city in terms of looking at how waste is dealt with currently.

Collection and awareness —- Transformation and use

She thought that although the connection between health, waste and food is something relevant that its a completely different project.

**Ramon and Lucas**

Look at economic factors and map out materials with various properties. Try to focus by piloting something here in the lab. Possible scenarios that this could be used for. How can we produce more biomas? Take a systems look that argues how to get hold of materials. Don’t worry about what you do with it, don’t become a materials engineer.

**Ingi**

Keep pursuing the saw dust boards, make a press and mix with other materials. Just move the designs forward more. Do more tests. Make a better press out of steel. You need to more quickly though as you don't really have very much time left.

<br>


<iframe width="640" height="360" src="https://www.youtube.com/embed/9E3fMglBNho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<a name="Reflections"></a>
## Reflections


I’m going to honest in this reflection. I’ve struggled this term. I’ve struggled to position myself within the work that other people are doing. I feel the issues surrounding materials and waste are important, and the fact that so many other people are examining the same things provides validation for me that I’m examining challenging issues. Ones which don’t have quick, easy, obvious solutions. My biggest problem this term has been a bad case of ‘decidophobia’, where I haven’t really made any important decisions, gone with one idea and stuck with it. I’ve highlighted problems, but still not proposed any concrete actions to move forward with. I’m running out of time, and my fear about doing things has had the effect of paralysing, and not moving forward to an concrete intervention. This is bad position to be starting the final term.

I like the idea of making more materials out of sawdust. Although i really am not sure about the impact of such a lot of effort. To get a board that is ultimately down-cycled material. That needs energy, time and materials put into it.  Is that more attractive than just buying an industrially made board currently. Probably not. I don't think that it will ultimately change much, because the incentives aren't really there to reuse the waste in that way. A more fundamental change will be needed. 


<a href="#Back to top">Back to top</a>

<a name="Research"></a>
## Research

**Organisations doing related work**:

- [Material driven](https://www.materialdriven.com)
- [Materiom](https://materiom.org)
- [Matefad](http://es.materfad.com)
- [Material Design Lab](http://materialdesignlab.dk)
- [Material Connections](https://www.materialconnexion.com)
- [Center for Advanced Materials & Manufacturing Innovation](https://ceps.unh.edu/center-advanced-materials-manufacturing-innovation)
- [Material District](https://materialdistrict.com)
- [Institute of Making](https://www.instituteofmaking.org.uk/materials-library)
- [California College of the Arts Material Library ](https://libraries.cca.edu/collections/special-collections/materials-library/)
- [Mediated Matter Group](https://mediatedmattergroup.com)
- [Materials today](https://www.materialstoday.com/news/)

- [Disruptive innovation festival DIF](https://www.thinkdif.co)
- Transitioning to circular economies [Ellen Macarthur Foundation](https://www.ellenmacarthurfoundation.org)
- [transmaterial.net](http://transmaterial.net)

**References, Articles and Papers**:

- Cindy Kohlata: Making Sustainability: How Fab Labs Address Environmental Sustainability, 2016
- Prof. Dr. Ann-Sophie Lehmann: Cube of Wood. Material Literacy for Art History, 2016
- [Ceschin, F., & Gaziulusoy, I. (2016). Evolution of design for sustainability: From product design to design for system innovations and transitions. Design Studies, 47, 118-163. ](https://research.aalto.fi/files/11561460/1_s2.0_S0142694X16300631_main.pdf)
- [Tim Ingold: An Ecology of Materials](https://www.diaphanes.net/titel/an-ecology-of-materials-3064)
- [S. E. Wilkes & M. A. Miodownik: Materials library collections as tools for interdisciplinary research. Interdisciplinary Science Reviews, Volume 43, 2018 - Issue 1, 2018](https://www.tandfonline.com/doi/full/10.1080/03080188.2018.1435450)
- [Neri Oxman, Material Ecology](http://neri.media.mit.edu/assets/pdf/Publications_ME.pdf)

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/lang/en/neri_oxman_design_at_the_intersection_of_technology_and_biology" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

- [The power of inconvience](https://medium.com/s/user-friendly/the-power-of-inconvenience-f0ae1773dd77)
- https://www.e-flux.com/announcements/29290/toms-saraceno/
- [Western melacholy how to imagine differnet futures in the real world](http://interakcije.net/en/2018/08/27/western-melancholy-how-to-imagine-different-futures-in-the-real-world/)
- [Recycling projects on desginboom](https://www.designboom.com/tag/recycling/)

Circular economies and sustainability:
- **[Circular Design Guide](https://www.circulardesignguide.com)**


Creative Commons:
- **[P2P Foundation](https://p2pfoundation.net)**

- Tracking waste
[Trash Track](http://senseable.mit.edu/trashtrack/index.php)

- Cosmolocalism:

"Cosmolocalism will document, analyze, test, evaluate, and create awareness about an emerging mode of production, based on the confluence of the digital commons (e.g., open knowledge and design) with local manufacturing and automation technologies"

"Cosmolocalism has three concurrent streams: democratization; innovation; and sustainability.""

<br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/MaAOryYIII4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**What are other maker spaces doing?**

I am fortunate to be interested in a subject in which i can get direct access to. Not only in the FabLab in IAAC, but also other maker spaces in Poblenou and Barcelona as a whole. I need to see what other people are doing, and gain information that might support or contradict my own experiences. Here are some places that are will visit.

- [Transfolab](https://www.transfolabbcn.com/home)
- [La Fabrica del Sol](http://ajuntament.barcelona.cat/lafabricadelsol/en)
- [Cannova - Ecolab + Bio Architecture](https://www.cannova.net)
- [Barcelona Wood Workshops](https://www.barcelonawoodworkshops.com/en/)
- [Made BCN Maker Space](http://made-bcn.org)



<a href="#Back to top">Back to top</a>
